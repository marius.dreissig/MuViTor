/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.resources;

import com.jme3.audio.AudioBuffer;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.shaman.muvitor.TimelineDisplay;

/**
 *
 * @author Sebastian
 */
public class AudioWaveformCreator {
	private static final Logger LOG = Logger.getLogger(AudioWaveformCreator.class.getName());
	public static final String FILE_SUFFIX = ".wfm";
	
	public static class WaveformFile implements Serializable {
		private int numSamples;
		private int framerate;
		private WaveformZoomLayer[] layers;

		public int getFramerate() {
			return framerate;
		}

		public WaveformZoomLayer[] getLayers() {
			return layers;
		}

		public int getNumSamples() {
			return numSamples;
		}
		
	}
	public static class WaveformZoomLayer implements Serializable {
		private float pixelsPerFrame;
		private byte[] values;

		public float getPixelsPerFrame() {
			return pixelsPerFrame;
		}

		public byte[] getValues() {
			return values;
		}
		
	}
	
	public static WaveformFile loadWaveform(File file) throws IOException, ClassNotFoundException {
		try (ObjectInputStream in = new ObjectInputStream(
				new GZIPInputStream(new BufferedInputStream(new FileInputStream(file))))) {
			return (WaveformFile) in.readObject();
		}
	}
	public static void saveWaveform(File file, WaveformFile waveform) throws IOException {
		try (ObjectOutputStream out = new ObjectOutputStream(
				new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(file))))) {
			out.writeObject(waveform);
		}
	}
	
	public static WaveformFile createWaveform(AudioBuffer buffer, int projectFramerate) {
		assert (TimelineDisplay.ZOOM_STEP == 2) : "Zoom step must be 2, otherwise the downsampling won't work";
		LOG.info("create waveform for "+buffer.toString());
		
		WaveformFile file = new WaveformFile();
		file.framerate = projectFramerate;
		
		//get buffer
		ByteBuffer data = buffer.getData();
		data.rewind();
		int channels = buffer.getChannels();
		int bitsPerSample = buffer.getBitsPerSample();
		file.numSamples = data.remaining() / channels / (bitsPerSample / 8);
		
		//convert whole resolution to a byte array
		int[] fullRes = new int[file.numSamples];
		int maxValue = 0;
		for (int i=0; i<file.numSamples; ++i) {
			float value = 0;
			for (int c=0; c<channels; ++c) {
				switch (bitsPerSample) {
					case 8:
						value = Math.max(value, Math.abs((data.get()&0xff) / 255.0f));
						break;
					case 16:
						value = Math.max(value, Math.abs(data.getShort() / (float)Short.MAX_VALUE));
						break;
					default:
						throw new IllegalArgumentException("unsupported BitsPerSample: "+bitsPerSample);
				}
			}
			int val = (int) (Math.min(value, 1) * 255);
			fullRes[i] = val;
			maxValue = Math.max(maxValue, val);
		}
		LOG.log(Level.INFO, "created full resolution buffer: {0} samples with a sample rate of {1}", new Object[]{fullRes.length, buffer.getSampleRate()});
		
		//downsample values
		ArrayList<WaveformZoomLayer> layers = new ArrayList<WaveformZoomLayer>();
		//1. layer
		int sampleRate = buffer.getSampleRate();
		float samplesPerPixel = (sampleRate / (TimelineDisplay.MAX_PIXELS_PER_FRAME * projectFramerate));
		WaveformZoomLayer layer = new WaveformZoomLayer();
		layer.pixelsPerFrame = TimelineDisplay.MAX_PIXELS_PER_FRAME;
		layer.values = new byte[file.numSamples / (int) samplesPerPixel];
		float scale = 255.0f / maxValue;
		for (int i=0; i<layer.values.length; ++i) {
			int max = 0;
			for (int j=(int) (i * samplesPerPixel); (j<(i+1)*samplesPerPixel) && (j<fullRes.length); ++j) {
				max = Math.max(max, fullRes[j]);
			}
			layer.values[i] = (byte) (int) (max * scale);
		}
		layers.add(layer);
		LOG.log(Level.INFO, "first layer created with {0} samples per pixel and a scaling of {1}", new Object[]{samplesPerPixel, scale});
		// all other layers
		while (true) {
			WaveformZoomLayer oldLayer = layers.get(layers.size()-1);
			if (oldLayer.pixelsPerFrame <= TimelineDisplay.MIN_PIXELS_PER_FRAME) break;
			WaveformZoomLayer newLayer = new WaveformZoomLayer();
			newLayer.pixelsPerFrame = oldLayer.pixelsPerFrame / 2;
			newLayer.values = new byte[oldLayer.values.length / 2];
			for (int i=0; i<newLayer.values.length; ++i) {
				newLayer.values[i] = (byte) (Math.max(oldLayer.values[2*i] & 0xff, oldLayer.values[2*i+1] & 0xff));
			}
			layers.add(newLayer);
		}
		//set to file
		file.layers = layers.toArray(new WaveformZoomLayer[layers.size()]);
		LOG.info("all "+file.layers.length+" zoom layers created");
		
		return file;
	}
}
