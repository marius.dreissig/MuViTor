/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.player;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioBuffer;
import com.jme3.audio.AudioData;
import com.jme3.audio.AudioKey;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.commons.io.IOUtils;
import org.shaman.muvitor.Settings;
import org.shaman.muvitor.MusicVideoEditor;
import org.shaman.muvitor.resources.VideoResource;
import org.shaman.muvitor.videoio.HumbleVideoFrameProvider;

/**
 *
 * @author Sebastian Weiss
 */
public class VideoTools {
	private static final Logger LOG = Logger.getLogger(VideoTools.class.getName());
	
	private static final String FFMPEG_KEY = "ffmpeg";
	public static File FFMPEG_FILE;
	private static final String AUDIO_SUFFIX = "audio.ogg";
	private static final String IMAGES_SUFFIX = ".png";
	private static final String THUMBNAILS_ZIP_SUFFIX = "thumbs.zip";
	private static final int MAX_THUMBNAIL_SIZE = 200;
	
	private final File projectFolder;
	private final String basePath;
	private int startFrame = -1;
	private int frameCount = -1;
	private ZipFile thumbsZip;
	
	public static void checkFfmpeg(Component parent) {
		if (FFMPEG_FILE != null && FFMPEG_FILE.exists()) {
			return;
		}
		String path = Settings.get(FFMPEG_KEY, null);
		if (path == null || !new File(path).exists() || !new File(path).canExecute()) {
			LOG.info("no ffmpeg stored in settings, open dialog");
			JFileChooser fc = new JFileChooser();
			fc.addChoosableFileFilter(new FileNameExtensionFilter("executables", "exe"));
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fc.setDialogTitle("Select ffmpeg.exe");
			int ret = fc.showOpenDialog(parent);
			if (ret == JFileChooser.APPROVE_OPTION) {
				File selected = fc.getSelectedFile();
				if (selected.canExecute()) {
					FFMPEG_FILE = selected;
					Settings.set(FFMPEG_KEY, selected.getAbsolutePath());
					LOG.log(Level.INFO, "ffmpeg found: {0}", selected);
				}
			}
		} else {
			FFMPEG_FILE = new File(path);
			LOG.log(Level.INFO, "ffmpeg found in settings: {0}", path);
		}
	}

	public VideoTools(String basePath, File projectFolder) {
		this.basePath = basePath;
		this.projectFolder = projectFolder;
	}

	public String getBasePath() {
		return basePath;
	}
	
	public String getAudioPath() {
		return basePath + File.separator + AUDIO_SUFFIX;
	}
	
	private ZipFile openZipFile(String path) throws IOException {
		return new ZipFile(path);
	}
	
	public String getThumbnailImagesPath() {
		return projectFolder.getAbsolutePath() + File.separator + basePath + File.separator + THUMBNAILS_ZIP_SUFFIX;
	}
	
	public int getFrameCount() throws IOException {
		if (frameCount >= 0) {
			return frameCount;
		}
		if (thumbsZip == null) {
			thumbsZip = openZipFile(getThumbnailImagesPath());
		}
		frameCount = thumbsZip.size();
		return frameCount;
	}
	
	private int getStartFrame() throws IOException {
		if (startFrame >= 0) {
			return startFrame;
		}
		if (thumbsZip == null) {
			thumbsZip = openZipFile(getThumbnailImagesPath());
		}
		int minIndex = Integer.MAX_VALUE;
		for (Enumeration<? extends ZipEntry> e = thumbsZip.entries(); e.hasMoreElements(); ) {
			ZipEntry entry = e.nextElement();
			int index = Integer.parseInt(entry.getName().substring(0, entry.getName().indexOf('.')));
			minIndex = Math.min(index, minIndex);
		} 
		startFrame = minIndex;
		return minIndex;
	}
	
	public BufferedImage getThumbnail(int frame) throws IOException {
		frame += getStartFrame();
		if (thumbsZip == null) {
			thumbsZip = openZipFile(getThumbnailImagesPath());
		}
		try (InputStream in = thumbsZip.getInputStream(thumbsZip.getEntry(frame+IMAGES_SUFFIX))) {
			return ImageIO.read(in);
		}
	}
	
	public BufferedImage getFullResImage(int frame) throws IOException {
		return ImageIO.read(new File(basePath + File.separator + (frame+getStartFrame()) + IMAGES_SUFFIX));
	}
	
	public static float getThumbnailScale(BufferedImage img) {
		int maxS = Math.max(img.getWidth(), img.getHeight());
		if (maxS <= MAX_THUMBNAIL_SIZE) {
			return 1;
		}
		float scale = MAX_THUMBNAIL_SIZE / (float) maxS;
		return scale;
	}
	
	public static BufferedImage scaleImage(BufferedImage img, float scale) {
		int newWidth = (int) (img.getWidth() * scale);
		int newHeight = (int) (img.getHeight()* scale);
		BufferedImage resized = new BufferedImage(newWidth, newHeight, img.getType());
		Graphics2D g = resized.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
			RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.drawImage(img, 0, 0, newWidth, newHeight, 0, 0, img.getWidth(),
			img.getHeight(), null);
		g.dispose();
		return resized;
	}
	
	public static BufferedImage ensureFormat(BufferedImage img, int format) {
		if (img.getType() == format) {
			return img;
		} else {
			BufferedImage i = new BufferedImage(img.getWidth(), img.getHeight(), format);
			Graphics g = i.getGraphics();
			g.drawImage(img, 0, 0, null);
			g.dispose();
			return i;
		}
	}
	
	private static class VideoCopyWorker extends SwingWorker<Void, Void> {

		private JDialog dialog;
		private JLabel message;
		private JProgressBar progress;
		private volatile boolean cancelled = false;
		
		private final File videoFile;
		private final String root;
		private final VideoResource res;

		public VideoCopyWorker(File videoFile, String root, VideoResource res) {
			this.videoFile = videoFile;
			this.root = root;
			this.res = res;
		}
		
		private void createDialog() {
			//create dialog
			dialog = new JDialog(MusicVideoEditor.MAIN_FRAME, "Importing \""+videoFile.getName()+"\"", true);
			message = new JLabel("Load project");
			message.setPreferredSize(new Dimension(400, 50));
			progress = new JProgressBar(0, 1000);
			progress.setIndeterminate(true);
			dialog.setLayout(new BorderLayout());
			dialog.add(message, BorderLayout.NORTH);
			dialog.add(progress, BorderLayout.CENTER);
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener((ActionEvent ae) -> {
				cancelled = true;
			});
			dialog.add(cancelButton, BorderLayout.SOUTH);
			dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
			dialog.setResizable(false);
			dialog.pack();
			dialog.setLocationRelativeTo(null);
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			//load video
			HumbleVideoFrameProvider frameProvider = new HumbleVideoFrameProvider(videoFile.getAbsolutePath());
			//write files into zip files
			LOG.info("write thumbnails");
			int frameCount = 0;
			setMessage("Create Thumnails: 0 frames processed (0 sec)");
			try (ZipOutputStream zipLow = new ZipOutputStream(new FileOutputStream(new File(root+File.separator+THUMBNAILS_ZIP_SUFFIX)))) {
				BufferedImage img = null;
				while ((img = frameProvider.getCurrentFrame(img)) != null) {
					if (cancelled) return null;
					img = ensureFormat(img, BufferedImage.TYPE_4BYTE_ABGR);
					//write thumbnail
					img = scaleImage(img, getThumbnailScale(img));
					zipLow.putNextEntry(new ZipEntry(frameProvider.getCurrentFrameIndex()+".png"));
					ImageIO.write(img, "png", zipLow);
					zipLow.flush();
					zipLow.closeEntry();
					//next image
					frameProvider.advance();
					//log
					frameCount++;
					if (frameCount % res.getFramerate() == 0) {
						setMessage("Create Thumnails " + frameCount + " frames processed (" + (frameCount / res.getFramerate()) + " sec)");
					}
				}
			}
			LOG.info("frames compressed and written to zip file");

			//initialize resource
			res.setWidth(frameProvider.getWidth());
			res.setHeight(frameProvider.getHeight());
			res.setHighResFile(videoFile.getAbsolutePath());

			setMessage("Extract audio");
			//call ffmpeg to extract audio
			String[] args = new String[]{
				FFMPEG_FILE.getAbsolutePath(),
	//			"-ss", startTime,
				"-i", videoFile.getAbsolutePath(),
				"-vn", "-acodec", "libvorbis",
	//			"-n",
				root + File.separator + AUDIO_SUFFIX
			};
			ProcessBuilder pb = new ProcessBuilder(args).inheritIO();
			LOG.info("start ffmpeg to extract audio: "+pb.command());
			Process p = pb.start();
			int exit = p.waitFor();
			if (exit != 0) {
				throw new IOException("ffmpeg terminated with a failure");
			}
			LOG.info("audio extracted");
			
			return null;
		}
		
		void setMessage(String message) {
			this.message.setText(message);
		}

		@Override
		protected void done() {
			super.done();
			dialog.setVisible(false);
			dialog.dispose();
		}
	}

	public static VideoResource copyVideoIntoProject(File videoFile, File directory, String baseName,
			int framerate, String resourceName) throws IOException, InterruptedException {
		
		String root = directory.getAbsolutePath() + File.separator + baseName;
		File rootDir = new File(root);
		if (!rootDir.mkdir()) {
			throw new IOException("unable to create output folder");
		}
		
		//create video resource
		VideoResource res = new VideoResource(resourceName);
		res.setFramerate(framerate);
		
		///call worker
		VideoCopyWorker worker = new VideoCopyWorker(videoFile, root, res);
		worker.createDialog();
		worker.execute();
		worker.dialog.setVisible(true);
		if (worker.cancelled) return null;
		
		return res;
	}
}
