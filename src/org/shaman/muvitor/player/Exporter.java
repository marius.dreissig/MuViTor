/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.player;

import com.jme3.system.AppSettings;
import com.jme3.system.JmeSystemDelegate;
import com.jme3.system.Timer;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.apache.commons.io.FileUtils;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.MusicVideoEditor;
import org.shaman.muvitor.Project;
import static org.shaman.muvitor.player.VideoTools.FFMPEG_FILE;
import org.shaman.muvitor.resources.AudioResource;
import org.shaman.muvitor.resources.Resource;
import org.shaman.muvitor.timeline.ResourceTimelineObject;
import org.shaman.muvitor.timeline.TimelineChannel;
import org.shaman.muvitor.timeline.TimelineObject;
import org.shaman.muvitor.videoio.HumbleVideoWriter;

/**
 *
 * @author Sebastian Weiss
 */
public class Exporter extends SwingWorker<Void, Void> {
	private static final Logger LOG = Logger.getLogger(Exporter.class.getName());
	
	private final Player player;
	private final File targetFile;
	private final Project project;
	private final FrameTime start;
	private final FrameTime end;
	private JDialog dialog;
	private JLabel message;
	private JProgressBar progress;
	private JButton cancelButton;
	private AtomicBoolean cancelled;
	
	public Exporter(Player player, File targetFile, FrameTime start, FrameTime end) {
		this.player = player;
		this.targetFile = targetFile;
		this.project = player.getProject();
		this.start = start;
		this.end = end;
		cancelled = new AtomicBoolean(false);
	}
	
	private void createDialog() {
		//create dialog
		dialog = new JDialog(MusicVideoEditor.MAIN_FRAME, "Exporting", true);
		message = new JLabel("Load project");
		message.setPreferredSize(new Dimension(300, 50));
		progress = new JProgressBar(0, 1000);
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener((ActionEvent ae) -> {
			cancelled.set(true);
		});
		dialog.setLayout(new BorderLayout());
		dialog.add(message, BorderLayout.NORTH);
		dialog.add(progress, BorderLayout.CENTER);
		dialog.add(cancelButton, BorderLayout.SOUTH);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setResizable(false);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
	}
	
	public static void exportProject(Player player, File targetFile, FrameTime start, FrameTime end) {
		Exporter e = new Exporter(player, targetFile, start, end);
		e.createDialog();
		e.execute();
		e.dialog.setVisible(true);
	}

	@Override
	protected Void doInBackground() throws Exception {
		//clean output folder
		File outputFolder = new File(project.getFolder(), "output");
		if (!outputFolder.exists()) {
			outputFolder.mkdir();
		}

		FrameTime ft;
		final float lengthFrames = end.toFrames() - start.toFrames();
		
		//write images
		String videoPath = outputFolder.getAbsolutePath()+File.separator+"video.mkv";
		File videoFile = new File(videoPath);
		boolean writeFrames = true;
		if (videoFile.exists()) {
			int ret = JOptionPane.showConfirmDialog(MusicVideoEditor.MAIN_FRAME, 
					"<html>A video file with the frames already exists. Do you want to overwrite it?<br>"
							+ "Choose 'no' if you only changed the audio data and the video is unchanged.</html>", 
					"Overwrite video file?", JOptionPane.YES_NO_CANCEL_OPTION);
			if (ret == JOptionPane.CANCEL_OPTION) {
				LOG.info("export cancelled");
				return null;
			}
			if (ret == JOptionPane.YES_OPTION) {
				LOG.info("delete existing video file");
				videoFile.delete(); //to be save
			} else {
				//no
				writeFrames = false;
			}
		}
		if (writeFrames) {
			setMessage("write frames");
			player.setRecordFrames(true);
			setProgress(1, 0);
			BufferedImage frame = new BufferedImage(project.getWidth(), project.getHeight(), BufferedImage.TYPE_INT_ARGB);
			int i=1;
			ft = start;
			try (HumbleVideoWriter writer = new HumbleVideoWriter(videoPath, 
					project.getFramerate(), project.getWidth(), project.getHeight(),
					"matroska", "libx264")) {
				Graphics2D g = frame.createGraphics();
				while (ft.compareTo(end) < 0) {
					player.renderFrame(ft, g);
					writer.addImage(frame);
					LOG.log(Level.INFO, "frame {0} processed", i);
					setMessage("write frames: "+ft.toString());
					ft = ft.increment();
					setProgress(1, i / lengthFrames);
					i++;
					if (cancelled.get()) {
						LOG.info("cancelled");
						break;
					}
				}
				g.dispose();
			} catch (Exception ex) {
				LOG.log(Level.SEVERE, "Unable to save frames", ex);
			}
			player.setRecordFrames(false);
			LOG.info("frames written");
		}
		
		//write audio
		//only one audio source is supported
		String audioPath = null;
		FrameTime offset = null;
		//find first active audio
		searchAudio:
		for (TimelineChannel c : project.getTimelineChannels()) {
			if (c.getState() == TimelineChannel.State.OFF) continue;
			for (TimelineObject o : c.getObjects()) {
				if (!o.isEnabled()) continue;
				if (!(o instanceof ResourceTimelineObject)) continue;
				ResourceTimelineObject obj = (ResourceTimelineObject) o;
				if (!(obj.getResource() instanceof Resource.AudioProvider)) continue;
				//we found the resource
				audioPath = project.getFolder().getAbsolutePath() + File.separator + ((Resource.AudioProvider) obj.getResource()).getAudioKey().getName();
				offset = obj.getStart().add(obj.getOffset()).subtract(start);
				break searchAudio;
			}
		}
		
		if (audioPath != null) {
			//assemble audio and video
			setMessage("write video file");
			setProgress(3, -1);
			String[] args = {
				FFMPEG_FILE.getAbsolutePath(),
				"-y",
				"-i", videoPath,
				"-ss", String.valueOf(-offset.toFrames()/50.0f),
				"-i", audioPath,
				"-c", "copy",
				"-map", "0:v",
				"-map", "1:a",
				"-bsf:v", "h264_mp4toannexb",
				"-r", String.valueOf(project.getFramerate()),
				"-shortest",
				targetFile.getAbsolutePath()
			};
			ProcessBuilder pb = new ProcessBuilder(args).inheritIO();
			LOG.info("start ffmpeg: "+pb.command());
			Process p = pb.start();
			int exit = p.waitFor();
			if (exit != 0) {
				throw new IOException("ffmpeg terminated with a failure");
			}
		} else {
			//no audio found, just copy to output folder
			//TODO: container conversion is missing
			FileUtils.copyFile(new File(videoPath), targetFile);
		}
		LOG.info("video file written to "+targetFile);
		
		return null;
	}

	void setMessage(String message) {
		this.message.setText(message);
	}
	void setProgress(int step, float progress) {
		if (progress < 0) {
			this.progress.setIndeterminate(true);
		} else {
			this.progress.setIndeterminate(false);
		}
		int p = (step-1)*1000 + (int)(progress * 1000);
		this.progress.setValue(p);
	}

	@Override
	protected void done() {
		super.done();
		dialog.setVisible(false);
		dialog.dispose();
	}
}
