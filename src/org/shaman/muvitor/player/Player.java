/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.player;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext;
import com.jme3.texture.FrameBuffer;
import com.jme3.texture.Image;
import com.jme3.util.BufferUtils;
import com.jme3.util.Screenshots;
import org.shaman.muvitor.Project;
import org.shaman.muvitor.resources.Resource;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.filters.FilterExecutor;
import org.shaman.muvitor.timeline.TimelineChannel;
import org.shaman.muvitor.timeline.TimelineObject;

/**
 *
 * @author Sebastian Weiss
 */
public class Player extends SimpleApplication implements PropertyChangeListener {

	private static final Logger LOG = Logger.getLogger(Player.class.getName());
	
	private static final int AUDIO_CLOCK_RESOLUTION = 10; //every 10 msec
	public static final String AUDIO_CONTROL = "audioControl";
	public static final String IMAGE_CONTROL = "imageControl";
	
	public static final String PROP_PLAYING = "playing";
	
	private final Project project;
	private final UndoableEditSupport undoSupport;
	private final ResourceLoaderImpl resourceLoader;
	private ResourceLoadingCallback resourceLoadingCallback;
	private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	
	private FrameBuffer screenFrameBuffer;
	private ByteBuffer screenOutputBuffer;
	private Node screenNode;
	private ViewPort screenViewPort;
	private BufferedImage screenAwtImage;
	private Consumer<FrameTime> renderListener;
	private FilterExecutor filterExecutor;
	
	private boolean highResPreview;
	private boolean initialized;
	private boolean playing;
	private final AtomicBoolean recordFrames = new AtomicBoolean(false);
	private final AtomicBoolean recordAudio = new AtomicBoolean(false);
	private long playingStartTime;
	private FrameTime playingStartFrame;
	private FrameTime currentTime;

	public Player(Project project, UndoableEditSupport undoSupport) {
		this.project = project;
		this.undoSupport = undoSupport;
		project.addPropertyChangeListener(this);
		
		resourceLoader = new ResourceLoaderImpl();
		currentTime = new FrameTime(project.getFramerate());
		
		screenAwtImage = new BufferedImage(project.getWidth(), project.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
		
		//app settings
		setShowSettings(false);
		AppSettings settings = new AppSettings(true);
		settings.setFrameRate(project.getFramerate());
		settings.setFrequency(project.getFramerate());
		settings.setVSync(false);
		settings.setWidth(project.getWidth());
		settings.setHeight(project.getHeight());
		setSettings(settings);
		start(JmeContext.Type.OffscreenSurface, true);
	}

	/**
	 * Sets the listener that is notified whenever a frame has completed to render.
	 * The method {@link #draw(java.awt.Graphics2D) } should then be called
	 * on the AWT-Event-Thread.
	 * @param renderListener the render listener
	 */
	public void setRenderListener(Consumer<FrameTime> renderListener) {
		this.renderListener = renderListener;
	}
	Consumer<FrameTime> getRenderListener() {
		return renderListener;
	}

	public boolean isHighResPreview() {
		return highResPreview;
	}

	public void setHighResPreview(boolean highResPreview) {
		this.highResPreview = highResPreview;
		LOG.info("use high res preview: "+highResPreview);
	}

	@Override
	public void simpleInitApp() {
		assetManager.registerLocator(project.getFolder().getAbsolutePath(), FileLocator.class);
		
		//create render viewport
		screenFrameBuffer = new FrameBuffer(project.getWidth(), project.getHeight(), 1);
		screenFrameBuffer.setColorBuffer(Image.Format.BGRA8);
		screenFrameBuffer.setDepthBuffer(Image.Format.Depth);
		screenNode = new Node("projectRoot");
		screenNode.setQueueBucket(RenderQueue.Bucket.Gui);
		screenViewPort = new ViewPort("offscreen", cam);
		screenViewPort = renderManager.createPreView("offscreen", guiViewPort.getCamera().clone());
		screenViewPort.attachScene(screenNode);
		screenViewPort.setOutputFrameBuffer(screenFrameBuffer);
		screenViewPort.setClearFlags(true, true, true);
		screenViewPort.setBackgroundColor(new ColorRGBA(
				project.getBackgroundColor().getRed()/255.0f, 
				project.getBackgroundColor().getGreen()/255.0f, 
				project.getBackgroundColor().getBlue()/255.0f, 
				0));
		screenOutputBuffer = BufferUtils.createByteBuffer(project.getWidth() * project.getHeight() * 4);
		
		//create filter executor
		filterExecutor = new FilterExecutor(renderManager);
	}

	@Override
	public void simpleUpdate(float tpf) {
		if (!initialized || isRecordFrames()) {
			super.simpleUpdate(tpf);
			return;
		}
		//update timing
		if (playing) {
			long elapsedTime = System.currentTimeMillis() - playingStartTime;
			currentTime = FrameTime.fromMillis((int) elapsedTime, project.getFramerate());
			currentTime = currentTime.add(playingStartFrame);
			project.setTime(currentTime);
		}
		
		callPreuUdateSteps();
		
		//call super
		super.simpleUpdate(tpf);
		
		updateScreenNode(tpf);
	}
	private void callPreuUdateSteps() {
		filterExecutor.newFrame(currentTime);
		for (TimelineObject.PreUpdateStep step : TimelineObject.PreUpdateStep.values()) {
			LOG.log(Level.FINE, "pre-update step {0}", step);
			for (TimelineChannel c : project.getTimelineChannels()) {
				if (c.isVisible()) {
					for (TimelineObject o : c.getObjects()) {
						o.performPreUpdateStep(this, step);
					}
				}
			}
		}
		LOG.fine("pre-update steps completed");
	}
	private void updateScreenNode(float tpf) {
		//update screen node
		screenNode.updateLogicalState(tpf);
		screenNode.updateGeometricState();
	}

	@Override
	public void simpleRender(RenderManager rm) {
		super.simpleRender(rm);
		if (!initialized) {
			return;
		}
		
		if (isRecordFrames()) return;
		
		renderScreenNode();
	}
	private void renderScreenNode() {
		//render scene
		renderManager.getRenderer().setBackgroundColor(screenViewPort.getBackgroundColor());
		renderManager.renderViewPort(screenViewPort, 1f / project.getFramerate());
		//copy scene to Swing
		renderManager.getRenderer().readFrameBufferWithFormat(screenFrameBuffer, screenOutputBuffer, Image.Format.BGRA8);
		synchronized(screenAwtImage) {
			Screenshots.convertScreenShot(screenOutputBuffer, screenAwtImage);
		}
		if (renderListener != null) {
			renderListener.accept(currentTime);
		}
	}

	public Project getProject() {
		return project;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener l) {
		propertyChangeSupport.addPropertyChangeListener(l);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener l) {
		propertyChangeSupport.removePropertyChangeListener(l);
	}
	
	/**
	 * Sets the selected time of playback in msec.
	 * @param msec 
	 */
	public void setTime(FrameTime msec) {
		currentTime = msec;
	}
	
	public FrameTime getTime() {
		return currentTime;
	}
	
	public boolean isPlaying() {
		return playing;
	}

	public boolean isRecordFrames() {
		return recordFrames.get();
	}
	public void setRecordFrames(boolean recording) {
		this.recordFrames.set(recording);
	}
	public boolean isRecordAudio() {
		return recordAudio.get();
	}
	public void setRecordAudio(boolean recording) {
		this.recordAudio.set(recording);
	}
	public boolean isUseHighResFrames() {
		return isRecordFrames() || highResPreview;
	}
	
	/**
	 * Starts the playback
	 */
	public void startPlayback() {
		if (playing) {
			LOG.warning("player is already playing");
			return;
		}
		propertyChangeSupport.firePropertyChange(PROP_PLAYING, false, true);
		playingStartTime = System.currentTimeMillis();
		playingStartFrame = currentTime;
		playing = true;
	}
	
	/**
	 * Stops the audio playback
	 */
	public void stopPlayback() {
		if (!playing) {
			LOG.warning("player was already stopped");
			return;
		}
		playing = false;
		propertyChangeSupport.firePropertyChange(PROP_PLAYING, true, false);
	}
	
	/**
	 * Draws the images. Should be called on a regular basis, optimally with the project frame rate
	 * @param g 
	 */
	public void draw(Graphics2D g) {
		Graphics2D g2d = (Graphics2D) g.create();
		
		//draw background
		g2d.setColor(project.getBackgroundColor());
		g2d.fillRect(0, 0, project.getWidth(), project.getHeight());
		
		//first test, no parallelism
		/*
		for (TimelineObject to : project.getTimelineObjects()) {
			PlayerImageControl pic = (PlayerImageControl) to.playerProperties.get(IMAGE_CONTROL);
			if (pic != null) {
				if (recording) {
					Image img = pic.computeFrame(currentTime, false);
					pic.drawFrame(g2d, img, false, false);
				} else {
					Image img = pic.computeFrame(currentTime, true);
					pic.drawFrame(g2d, img, true, selections.getSelectedTimelineObject()==to);
				}
			}
		}
		*/
		synchronized(screenAwtImage) {
			g2d.drawImage(screenAwtImage, 0, 0, null);
		}
		
		g2d.dispose();
	}
	
	public void renderFrame(FrameTime time, Graphics2D g) throws InterruptedException, ExecutionException {
		Callable<Object> c = () -> {
			currentTime = time;
			callPreuUdateSteps();
			updateScreenNode(1.0f / project.getFramerate());
			renderScreenNode();
			draw(g);
			return new Object();
		};
		enqueue(c).get();
	}
	
	/**
	 * Exports the project
	 * @param target the target file
	 * @param start start time
	 * @param end end time
	 */
	public void export(File target, FrameTime start, FrameTime end) {
		Exporter.exportProject(this, target, start, end);
	}

	/**
	 * Returns the filter executor used in the pre-update step.
	 * @return 
	 */
	public FilterExecutor getFilterExecutor() {
		return filterExecutor;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() == project) {
			switch (evt.getPropertyName()) {
				case Project.PROP_TIME:
					setTime((FrameTime) evt.getNewValue());
					break;
				case Project.PROP_RESOURCE_ADDED:
					loadResource((Resource) evt.getNewValue());
					break;
				case Project.PROP_RESOURCE_REMOVED:
					deleteResource((Resource) evt.getOldValue());
					break;
				case Project.PROP_TIMELINE_CHANNELS_CHANGED:
					computeTotalLength();
					break;
				case Project.PROP_BACKGROUNDCOLOR:
					screenViewPort.setBackgroundColor(new ColorRGBA(
						project.getBackgroundColor().getRed()/255.0f, 
						project.getBackgroundColor().getGreen()/255.0f, 
						project.getBackgroundColor().getBlue()/255.0f, 
						project.getBackgroundColor().getAlpha()/255.0f));
					break;
//				case Project.PROP_TIMELINE_CHANNEL_ADDED:
//					((TimelineChannel) evt.getNewValue()).addListener(this);
//					break;
//				case TimelineChannel.PROP_OBJECT_ADDED:
//					initTimelineObject((TimelineObject) evt.getOldValue());
//					break;
//				case TimelineChannel.PROP_OBJECT_REMOVED:
//					deleteTimelineObject((TimelineObject) evt.getOldValue());
//					break;
			}
		}
	}
	
	public static interface ResourceLoadingCallback {
		void onMessage(String message);
		void onProgress(float progress);
	}
	
	public void loadResources(ResourceLoadingCallback callback) {
		this.resourceLoadingCallback = callback;
		try {
			enqueue(() -> {
				//load resources
				if (callback != null) {callback.onProgress(0);}
				int i = 0;
				for (Resource r : project.getResources()) {
					loadResource(r);
					i++;
					if (callback != null) {callback.onProgress(i / (float)project.getResources().size());}
				}
				//init project
				project.initPlayer(this, screenNode);
				//done
				initialized = true;
				return new Object();
			}).get();
		} catch (InterruptedException | ExecutionException ex) {
			Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
		}
		this.resourceLoadingCallback = null;
	}
	public void loadResource(Resource res) {
		if (!res.isLoaded()) {
			res.load(resourceLoader);
		}
	}
	
	public void deleteResource(Resource res) {
		assert(res.isLoaded());
		res.unload();
	}
	
	/**
	 * Computes the total length of the playback
	 */
	private void computeTotalLength() {
		FrameTime length = project.computeTotalLength();
		LOG.log(Level.FINE, "length of the project: {0}", length);
		project.setLength(length);
	}
	
	private class ResourceLoaderImpl implements Resource.ResourceLoader {

		@Override
		public File getProjectDirectory() {
			return project.getFolder();
		}

		@Override
		public void setMessage(String message) {
			LOG.info("loading: "+message);
			if (resourceLoadingCallback != null) {
				resourceLoadingCallback.onMessage(message);
			}
		}

		@Override
		public Player getPlayer() {
			return Player.this;
		}

	}
	
}
