/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import org.shaman.muvitor.filters.Filter;
import org.shaman.muvitor.layout.Layout;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.resources.AudioResource;
import org.shaman.muvitor.resources.Resource;
import org.shaman.muvitor.resources.TimelinePreviewFactory;
import org.shaman.muvitor.resources.VideoResource;
import org.shaman.muvitor.timeline.AudioTimelineObject;
import org.shaman.muvitor.timeline.ImageTimelineObject;
import org.shaman.muvitor.timeline.Marker;
import org.shaman.muvitor.timeline.ResourceTimelineObject;
import org.shaman.muvitor.timeline.TimelineChannel;
import org.shaman.muvitor.timeline.TimelineObject;

/**
 *
 * @author Sebastian
 */
public class TimelineDisplay extends JComponent 
		implements Scrollable, MouseListener, MouseMotionListener, PropertyChangeListener {
	public static final float MAX_PIXELS_PER_FRAME = 64; //must be a power of two
	public static final float MIN_PIXELS_PER_FRAME = 1.0f / 64;
	public static final float ZOOM_STEP = 2; //must be a power of two
	
	private static final Logger LOG = Logger.getLogger(TimelineDisplay.class.getName());
	private static final Color TIME_HANDLER_COLOR = new Color(255, 0, 0);
	private static final Color BACKGROUND_COLOR = new Color(50, 50, 50);
	private static final int SNAP_PIXEL_DISTANCE = 10;
	private static final int BOTH_SIDED_CURSOR_DISTANCE = -1;//10, disabled;
	private static final int ONE_SIDED_CURSOR_DISTANCE = 25;
	private static final int PREVIEW_INTENT = 2;
	private static final BufferedImage DEACTIVATED_PATTERN;
	private static final BufferedImage SELECTED_FILTER_BORDER;
	
	static {
		try {
			DEACTIVATED_PATTERN = ImageIO.read(TimelineDisplay.class.getClassLoader().getResource("org/shaman/muvitor/icons/deactivated_pattern.png"));
			SELECTED_FILTER_BORDER = ImageIO.read(TimelineDisplay.class.getClassLoader().getResource("org/shaman/muvitor/filters/marked_icon.png"));
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	private final Project project;
	private Player player;
	/**
	 * Pixels per frame
	 */
	private float pixelsPerFrame = 1;
	/**
	 * The width of the channel names in pixels
	 */
	private int channelNameWidth = 100;
	/**
	 * The height of the header in pixels
	 */
	private int headerHeight = 40;
	/**
	 * The height of each row in pixels, global for now.
	 */
	private int rowHeight = 40;
	
	private RowHeader rowHeader;
	private ColumnHeader columnHeader;
	private ImportHandler importHandler;
	private JScrollPane scrollPane;
	
	/**
	 * For rendering the frames and audio signals
	 */
	private TimelinePreviewFactory timelinePreviewFactory;
	
	//dnd
	private ResourceTimelineObject dndResource;
	private int dndChannel;
	private int dndFrame;
	
	//selection and transformation
	private final Cursor moveCursorBoth;
	private final Cursor moveCursorLeft;
	private final Cursor moveCursorRight;
	private final Cursor[] cursors;
	private int lastMouseButton;
	private ResourceTimelineObject selectedResource;
	private int selectedResourceIndex = -1;
	private TimelineChannel selectedChannel;
	private int selectedChannelIndex = -1;
	private ResourceTimelineObject clickedResource;
	private int clickedResourceIndex = -1;
	private TimelineChannel clickedChannel;
	private int clickedChannelIndex = -1;
	private Filter selectedFilter;
	private int selectedFilterIndex = -1;
	private Filter clickedFilter;
	/**
	 * The side: 1==left, 2==right, 0==in the middle (moving)
	 */
	private int selectionSide;
	/**
	 * The cursor: 0==regular, 1==left cursor / right side, 2==right cursor / left side, 3==both, 4==move
	 */
	private int selectionCursor;
	private Point referenceMouse;
	private FrameTime referenceTime;
	
	//for snapping
	private boolean snapping = false;
	private static class SnapPoint {
		private final int pixel;
		private final FrameTime time;
		private static interface Reference {}
		private static class TimelineReference implements Reference {
			private final TimelineObject obj;
			//false=left, true=right
			private final boolean side;
			private TimelineReference(TimelineObject obj, boolean side) {
				this.obj = obj;
				this.side = side;
			}
		}
		private static class MarkerReference implements Reference {
			private final Marker marker;
			private MarkerReference(Marker marker) {
				this.marker = marker;
			}
		}
		private final ArrayList<Reference> references;
		private SnapPoint(int pixel, FrameTime time) {
			this.pixel = pixel;
			this.time = time;
			references = new ArrayList<>();
		}
	}
	private TreeMap<Integer, SnapPoint> snapPoints;
	private SnapPoint selectedSnapPoint = null;
	
	//context menu
	public static interface ContextMenuListener {
		void onMenuOpen(Object selectedObj, Point mouse);
	}
	private ContextMenuListener contextMenuListener;
	
	private TimelineDisplay(Project project) {
		this.project = project;
		
		//create cursors
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		moveCursorLeft = toolkit.createCustomCursor(
				toolkit.getImage(TimelineDisplay.class.getClassLoader().getResource("org/shaman/muvitor/icons/timelineLeftCursor.png")),
				new Point(12, 12), "timeline move left");
		moveCursorRight = toolkit.createCustomCursor(
				toolkit.getImage(TimelineDisplay.class.getClassLoader().getResource("org/shaman/muvitor/icons/timelineRightCursor.png")),
				new Point(12, 12), "timeline move right");
		moveCursorBoth = toolkit.createCustomCursor(
				toolkit.getImage(TimelineDisplay.class.getClassLoader().getResource("org/shaman/muvitor/icons/timelineBothCursor.png")),
				new Point(12, 12), "timeline move both");
		cursors = new Cursor[] {
			Cursor.getDefaultCursor(),
			moveCursorLeft,
			moveCursorRight,
			moveCursorBoth,
			Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR)
		};
		snapPoints = new TreeMap<>();
		
		project.addPropertyChangeListener(this);
		
		importHandler = new ImportHandler();
		setDropTarget(new DropTarget(this, importHandler));
		
		addMouseListener(this);
		addMouseMotionListener(this);
		setFocusable(true);
		setBackground(BACKGROUND_COLOR);
		
		columnHeader = new ColumnHeader();
		rowHeader = new RowHeader();
		
		timelinePreviewFactory = new TimelinePreviewFactory(rowHeight - 2*PREVIEW_INTENT);
		timelinePreviewFactory.addPropertyChangeListener(this);
	}
	
	public static TimelineDisplay create(Project project, JScrollPane scrollPane) {
		TimelineDisplay d = new TimelineDisplay(project);
		scrollPane.setViewportView(d);
		d.scrollPane = scrollPane;
		
		scrollPane.setColumnHeaderView(d.columnHeader);
		scrollPane.setRowHeaderView(d.rowHeader);
		scrollPane.getViewport().setBackground(BACKGROUND_COLOR);
		
		d.update();
		
		return d;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void setContextMenuListener(ContextMenuListener contextMenuListener) {
		this.contextMenuListener = contextMenuListener;
	}
	
	public void setSnapping(boolean snapping) {
		this.snapping = snapping;
		if (!snapping) {
			selectedSnapPoint = null;
			snapPoints.clear();
		}
	}
	
	/**
	 * Zooms in
	 */
	public void zoomIn() {
		pixelsPerFrame *= ZOOM_STEP;
		pixelsPerFrame = Math.min(pixelsPerFrame, MAX_PIXELS_PER_FRAME);
		update();
		scrollViewport();
	}
	
	/**
	 * Zooms out
	 */
	public void zoomOut() {
		pixelsPerFrame /= ZOOM_STEP;
		pixelsPerFrame = Math.max(pixelsPerFrame, MIN_PIXELS_PER_FRAME);
		update();
		scrollViewport();
	}
	
	public void addChannel() {
		TimelineChannel c = new TimelineChannel();
		project.addTimelineChannel(c);
		update();
		LOG.info("a new channel added");
	}
	
	private int computeHeight() {
		return rowHeight * project.getTimelineChannels().size() + 2;
	}
	
	private int computeWidth() {
		FrameTime length = project.computeTotalLength();
		return Math.max((int) Math.ceil(length.toFrames() * pixelsPerFrame) + 2, 50);
	}
	
	/**
	 * Updates the bounds and repaints it
	 */
	public void update() {
		//updates this size
		setPreferredSize(new Dimension(computeWidth(), computeHeight()));
		System.out.println("size: "+getPreferredSize());
		
		//update headers
		rowHeader.update();
		columnHeader.update();
		
		//invalidate and repaint
		scrollPane.revalidate();
		
		rowHeader.repaint();
		columnHeader.repaint();
		repaint();
	}
	
	//------------------------------
	// SCROLL PANE
	//------------------------------
	

	/**
	 * If the current time is outside the visible rect, the viewport is scrolled
	 */
	private void scrollViewport() {
		Rectangle visibleRect = getVisibleRect();
		int targetX = (int) (project.getTime().toFrames() * pixelsPerFrame);
		if (targetX < visibleRect.x) {
			//scroll to the left
			visibleRect.x = Math.max(0, targetX - visibleRect.width * 3 / 4);
			scrollRectToVisible(visibleRect);
		} else if (targetX > visibleRect.x + visibleRect.width) {
			//scroll to the right
			visibleRect.x = targetX - visibleRect.width / 4;
			scrollRectToVisible(visibleRect);
		}
	}
	
	@Override
	public Dimension getPreferredScrollableViewportSize() {
		return getPreferredSize();
	}

	@Override
	public int getScrollableUnitIncrement(Rectangle rctngl, int i, int i1) {
		return 2;
	}

	@Override
	public int getScrollableBlockIncrement(Rectangle rctngl, int i, int i1) {
		return rowHeight;
	}

	@Override
	public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	@Override
	public boolean getScrollableTracksViewportHeight() {
		return false;
	}
	
	//------------------------------
	// MOUSE TRANSFORMATIONS
	//------------------------------
	
	private ResourceTimelineObject<?> createTimelineObject(Resource res) {
		ResourceTimelineObject obj = null;
		if (res instanceof AudioResource) {
			AudioTimelineObject o = new AudioTimelineObject((AudioResource) res);
			o.setDuration(FrameTime.fromFrames(
					(int)Math.ceil(((AudioResource)res).getDurationInFrames()), 
					project.getFramerate()));
			obj = o;
		} else if (res instanceof Resource.ImageProvider) {
			ImageTimelineObject o = new ImageTimelineObject(res);
			o.setWidth(((Resource.ImageProvider) res).getWidth());
			o.setHeight(((Resource.ImageProvider) res).getHeight());
			o.setAspect(o.getWidth() / (float) o.getHeight());
			if (res instanceof VideoResource) {
				o.setDuration(FrameTime.fromFrames(
						((VideoResource) res).getDurationInFrames(),
						project.getFramerate()));
			} else { //image
				o.setDuration(FrameTime.fromMillis(1000, project.getFramerate())); //1sec
			}
			obj = o;
		} else {
			LOG.log(Level.WARNING, "unknown resource type: {0}", res);
			return null;
		}
		obj.setStart(new FrameTime(project.getFramerate()));
		obj.setOffset(new FrameTime(project.getFramerate()));
		return obj;
	}
	
	private void dropCurrentObject() {
		TimelineChannel channel = project.getTimelineChannels().get(dndChannel);
		dndResource.setOffset(FrameTime.fromFrames(dndFrame, project.getFramerate()));
		
		//find object before and after
		int index = channel.getObjects().size();
		for (int i=0; i<channel.getObjects().size(); ++i) {
			ResourceTimelineObject obj = channel.getObjects().get(i);
			FrameTime end = obj.getOffset().add(obj.getDuration());
			if (end.compareTo(dndResource.getOffset()) > 0) {
				index = i;
				break;
			}
		} 
		
		//clip timeline objects before and after to fit in the new resource
		if (index < channel.getObjects().size()) {
			ResourceTimelineObject rightObj = channel.getObjects().get(index);
			FrameTime rightLimit = rightObj.getOffset().add(rightObj.getStart());
			FrameTime dur = rightLimit.subtract(dndResource.getOffset());
			if (dur.compareTo(dndResource.getDuration())<0) {
				dndResource.setDurationNoUndo(dur);
			}
		}
		
		//add resource to the channel
		channel.addTimelineObject(dndResource);
		
		//clear and repaint
		dndResource = null;
		repaint();
	}

	private int getChannelFromMouse(Point mouse) {
		int channel = mouse.y / rowHeight;
		if (channel < 0 || channel >= project.getTimelineChannels().size()) return -1;
		return channel;
	}
	private int getFramesFromMosue(Point mouse) {
		return (int) (mouse.x / pixelsPerFrame);
	}
	
	/**
	 * Creates the snap points.
	 * @param toIgnore the object under transformation, should be ignored
	 */
	private void createSnapPoints(Object toIgnore) {
		snapPoints.clear();
		selectedSnapPoint = null;
		//snap to timeline objects
		for (TimelineChannel c : project.getTimelineChannels()) {
			for (TimelineObject o : c.getObjects()) {
				if (o == toIgnore) continue;
				//left snap
				FrameTime left = o.getOffset().add(o.getStart());
				int leftPixels = (int) (left.toFrames() * pixelsPerFrame);
				SnapPoint leftPoint = snapPoints.get(leftPixels);
				if (leftPoint == null) {
					leftPoint = new SnapPoint(leftPixels, left);
					snapPoints.put(leftPixels, leftPoint);
				}
				leftPoint.references.add(new SnapPoint.TimelineReference(o, false));
				//right snap
				FrameTime right = o.getOffset().add(o.getDuration());
				int rightPixels = (int) (right.toFrames() * pixelsPerFrame);
				SnapPoint rightPoint = snapPoints.get(rightPixels);
				if (rightPoint == null) {
					rightPoint = new SnapPoint(rightPixels, right);
					snapPoints.put(rightPixels, rightPoint);
				}
				rightPoint.references.add(new SnapPoint.TimelineReference(o, true));
			}
		}
		//snap to markers
		for (Marker m : project.getMarkers()) {
			FrameTime t = m.getTime();
			int pixels = (int) (t.toFrames() * pixelsPerFrame);
			SnapPoint point = snapPoints.get(pixels);
			if (point == null) {
				point = new SnapPoint(pixels, t);
				snapPoints.put(pixels, point);
			}
			point.references.add(new SnapPoint.MarkerReference(m));
		}
	}
	
	/**
	 * Finds the closest snap point within the tolerance of {@link #SNAP_PIXEL_DISTANCE}
	 * @param mouseX the mouse x position
	 * @return the the closest snap point, or {@code null} if none is in reach
	 */
	private SnapPoint findClosestSnapPointIndex(int mouseX) {
		SnapPoint closest = null;
		int distance = Integer.MAX_VALUE;
		for (SnapPoint p : snapPoints.subMap(mouseX - SNAP_PIXEL_DISTANCE, true, mouseX + SNAP_PIXEL_DISTANCE, true).values()) {
			int dist = Math.abs(p.pixel - mouseX);
			if (dist < distance) {
				distance = dist;
				closest = p;
			}
		}
		return closest;
	}
	
	@Override
	public void mouseClicked(MouseEvent me) {
	}

	@Override
	public void mousePressed(MouseEvent me) {
//		if (me.isPopupTrigger()) {
		if (me.getButton() == MouseEvent.BUTTON3) {
			//right-click
			if (clickedFilter != null) {
				contextMenuListener.onMenuOpen(clickedFilter, me.getPoint());
			} else {
				contextMenuListener.onMenuOpen(clickedResource, me.getPoint());
			}
			return;
		}
		
		lastMouseButton = me.getButton();
		//if something is selected, copy it to the 'clicked' variables
		if (selectedFilter != null) {
			clickedFilter = selectedFilter;
			clickedResource = null;
			Selections.getInstance().setFocussedObject(selectedFilter);
			repaint();
		}
		else if (selectedResource != null) {
			clickedResource = selectedResource;
			clickedResourceIndex = selectedResourceIndex;
			clickedChannel = selectedChannel;
			clickedChannelIndex = selectedChannelIndex;
			clickedFilter = null;
			//store mouse and reference time
			referenceMouse = (Point) me.getPoint().clone();
			switch (selectionCursor) {
				case 2: //left cursor
					referenceTime = selectedResource.getStart();
					break;
				case 1: //duration
					referenceTime = selectedResource.getDuration();
					break;
				case 4: //move
					referenceTime = selectedResource.getOffset();
					break;
			}
			//compute snap points
			if (snapping) {
				createSnapPoints(selectedResource);
			}
			//set selection
			Selections.getInstance().setSelectedTimelineObject(clickedResource);
			Selections.getInstance().setFocussedObject(clickedResource);
			//repaint and log
			repaint();
			LOG.log(Level.INFO, "object {0} selected", clickedResource);
		} else {
			//unselect it
			clickedResource = null;
			clickedFilter = null;
			Selections.getInstance().setSelectedTimelineObject(null);
			Selections.getInstance().setFocussedObject(null);
			repaint();
		}
	}

	@Override
	public void mouseReleased(MouseEvent me) {
		lastMouseButton = 0;
		if (selectedResource == null) return;
		//clear snap points
		selectedSnapPoint = null;
		snapPoints.clear();
		//fire undo-redo
		switch (selectionCursor) {
			case 2: //left cursor
				selectedResource.fireStartUndoRedo(referenceTime, selectedResource.getStart());
				break;
			case 1: //right cursor
				selectedResource.fireDurationUndoRedo(referenceTime, selectedResource.getDuration());
				break;
			case 4: //move
				selectedResource.fireOffsetUndoRedo(referenceTime, selectedResource.getOffset());
				break;
		}
		selectedResource = null;
	}

	@Override
	public void mouseEntered(MouseEvent me) {
		
	}

	@Override
	public void mouseExited(MouseEvent me) {
		
	}

	@Override
	public void mouseDragged(MouseEvent me) {
		if (selectedResource == null) return;
		if (referenceTime == null) return; //unknown op
		if (lastMouseButton != MouseEvent.BUTTON1) return;
		//compute new values
		int offX = me.getPoint().x - referenceMouse.x;
		int offFrames = (int) (offX / pixelsPerFrame);
		int newValue = referenceTime.toFrames() + offFrames;
		//snapping
		if (snapping) {
			SnapPoint sp = findClosestSnapPointIndex(me.getPoint().x);
			if (sp != null) {
				selectedSnapPoint = sp;
				newValue = sp.time.toFrames();
				if (selectionCursor != 4) newValue -= selectedResource.getOffset().toFrames();
				offX = (int) Math.signum(newValue - referenceTime.toFrames());
			} else {
				selectedSnapPoint = null;
			}
		}
		//get left clamp
		int leftLimit = -1;
		if (offX < 0 && selectedResourceIndex > 0) {
			TimelineObject leftObj = selectedChannel.getObjects().get(selectedResourceIndex-1);
			leftLimit = leftObj.getOffset().toFrames() + leftObj.getDuration().toFrames();
		}
		//get right clamp
		int rightLimit = -1;
		if (offX > 0 && selectedResourceIndex < selectedChannel.getObjects().size() - 1) {
			TimelineObject rightObj = selectedChannel.getObjects().get(selectedResourceIndex+1);
			rightLimit = rightObj.getOffset().toFrames() + rightObj.getStart().toFrames();
		}
		//move
		switch (selectionCursor) {
			case 2: { //left
				if (leftLimit >= 0) {
					newValue = Math.max(newValue, leftLimit - selectedResource.getOffset().toFrames());
				}
				selectedResource.setStartNoRedo(FrameTime.fromFrames(newValue, project.getFramerate()));
			} break;
			case 1: { //right
				if (rightLimit >= 0) {
					newValue = Math.min(newValue, rightLimit - selectedResource.getOffset().toFrames());
				}
				selectedResource.setDurationNoUndo(FrameTime.fromFrames(newValue, project.getFramerate()));
			} break;
			case 4: { //move
				if (leftLimit >= 0) {
					newValue = Math.max(newValue, leftLimit - selectedResource.getStart().toFrames());
				}
				if (rightLimit >= 0) {
					newValue = Math.min(newValue, rightLimit - selectedResource.getDuration().toFrames());
				}
				selectedResource.setOffsetNoRedo(FrameTime.fromFrames(newValue, project.getFramerate()));
			} break;
		}
		repaint();
		columnHeader.repaint();
	}
	
	@Override
	public void mouseMoved(MouseEvent me) {
		//clear selection
		ResourceTimelineObject oldSelection = selectedResource;
		selectionCursor = 0;
		selectedChannel = null;
		selectedChannelIndex = -1;
		selectedResource = null;
		selectedResourceIndex = -1;
		selectionSide = 0;
		selectedFilter = null;
		selectedFilterIndex = -1;
		//check selection
		int channel = getChannelFromMouse(me.getPoint());
		int frames = getFramesFromMosue(me.getPoint());
		if (channel >= 0) {
			TimelineChannel c = project.getTimelineChannels().get(channel);
			for (int i=0; i<c.getObjects().size(); ++i) {
				ResourceTimelineObject obj = c.getObjects().get(i);
				int offset = obj.getOffset().toFrames();
				int start = obj.getStart().toFrames() + offset;
				int end = obj.getDuration().toFrames() + offset;
				if (Math.abs(start - frames)*pixelsPerFrame <= BOTH_SIDED_CURSOR_DISTANCE) {
					//both left
					selectedChannel = c;
					selectedResource = obj;
					selectedChannelIndex = channel;
					selectedResourceIndex = i;
					selectionSide = 1;
					selectionCursor = 3;
					break;
				}
				if (Math.abs(end - frames)*pixelsPerFrame <= BOTH_SIDED_CURSOR_DISTANCE) {
					//both right
					selectedChannel = c;
					selectedResource = obj;
					selectedChannelIndex = channel;
					selectedResourceIndex = i;
					selectionSide = 2;
					selectionCursor = 3;
					break;
				}
				if (start < frames && (frames - start)*pixelsPerFrame < ONE_SIDED_CURSOR_DISTANCE) {
					//left cursor right
					selectedChannel = c;
					selectedResource = obj;
					selectedChannelIndex = channel;
					selectedResourceIndex = i;
					selectionSide = 1;
					selectionCursor = 2;
					break;
				}
				if (end > frames && (end - frames)*pixelsPerFrame < ONE_SIDED_CURSOR_DISTANCE) {
					//right cursor left
					selectedChannel = c;
					selectedResource = obj;
					selectedChannelIndex = channel;
					selectedResourceIndex = i;
					selectionSide = 2;
					selectionCursor = 1;
					break;
				}
				if (start < frames && end > frames) {
					//middle
					selectedChannel = c;
					selectedResource = obj;
					selectedChannelIndex = channel;
					selectedResourceIndex = i;
					selectionSide = 0;
					selectionCursor = 4;
					break;
				}
			}
		}
		
		//check for filters
		if (selectedResource != null) {
			Filter[] fx = selectedResource.getFilterArray();
			int offsetInFrames = selectedResource.getOffset().toFrames();
			int startInFrames = selectedResource.getStart().toFrames();
			int beginX = (int) ((startInFrames + offsetInFrames) * pixelsPerFrame);
			int beginY = channel * rowHeight;
			Rectangle clip = getVisibleRect();
			int filterX = Math.max(beginX, clip.x);
			for (int i=0; i<fx.length; ++i) {
				int x = filterX + 5 + 26 * i + 12;
				int y = beginY + 5 + 12;
				float dist = (x-me.getX())*(x-me.getX()) + (y-me.getY())*(y-me.getY());
				if (dist <= 12*12) {
					selectedFilter = fx[i];
					selectedFilterIndex = i;
					selectionCursor = 0;
					break;
				}
			}
		}
		
		//set cursor
		setCursor(cursors[selectionCursor]);
		
		if (oldSelection != selectedResource) {
			repaint();
		}
	}
	
	//------------------------------
	// PROPERTY CHANGE LISTENER
	//------------------------------

	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (pce.getSource() == project) {
			switch (pce.getPropertyName()) {
				case Project.PROP_TIME:
				case Project.PROP_LENGTH:
				case Project.PROP_MARKER_CHANGED:
					repaint();
					columnHeader.repaint();
					scrollViewport();
					break;
				case Project.PROP_TIMELINE_CHANNELS_CHANGED:
				case Project.PROP_TIMELINE_CHANNEL_CHANGED:
					repaint();
					break;
				case Project.PROP_LAYOUTS_CHANGED:
					columnHeader.repaint();
			}
		} else if (pce.getSource() == timelinePreviewFactory) {
			//trigger repaint, a new resource is available
			repaint();
		}
	}
	
	//------------------------------
	// DRAG AND DROP
	//------------------------------
	
	private class ImportHandler implements DropTargetListener {

		@Override
		public void dragEnter(DropTargetDragEvent dtde) {
			Transferable transfer = dtde.getTransferable();
			if (!transfer.isDataFlavorSupported(ResourcePanel.ResourceTransferable.RESOURCE_DATA_FLAVOR)) {
				dtde.rejectDrag();
				return;
			}
			try {
				Resource res = (Resource) transfer.getTransferData(ResourcePanel.ResourceTransferable.RESOURCE_DATA_FLAVOR);
				LOG.info("importing "+res);
				//create timeline object
				dndResource = createTimelineObject(res);
				if (dndResource == null) {
					dtde.rejectDrag();
					return;
				}
				dndChannel = 0;
				dndFrame = 0;
			} catch (UnsupportedFlavorException | IOException ex) {
				Logger.getLogger(TimelineDisplay.class.getName()).log(Level.SEVERE, null, ex);
				dtde.rejectDrag();
				return;
			}
		}

		@Override
		public void dragOver(DropTargetDragEvent dtde) {
			//set start frame and channe
			Point p = dtde.getLocation();
			System.out.println("drag over "+p);
			dndChannel = Math.max(0, Math.min(project.getTimelineChannels().size()-1, p.y / rowHeight));
			dndFrame = (int) (p.x / pixelsPerFrame);
			
			//check if the position is allowed
			for (ResourceTimelineObject obj : project.getTimelineChannels().get(dndChannel).getObjects()) {
				int start = obj.getOffset().toFrames() + obj.getStart().toFrames();
				int end = obj.getOffset().toFrames() + obj.getDuration().toFrames();
				if (start <= dndFrame && dndFrame < end) {
					dtde.acceptDrag(DnDConstants.ACTION_NONE);
					dndFrame = -1;
					repaint();
					return;
				}
			}
			dtde.acceptDrag(DnDConstants.ACTION_COPY);
			
			repaint();
		}

		@Override
		public void dropActionChanged(DropTargetDragEvent dtde) {
			//do nothing
		}

		@Override
		public void dragExit(DropTargetEvent dte) {
			//clear resource
			dndResource = null;
			repaint();
		}

		@Override
		public void drop(DropTargetDropEvent dtde) {
			if (dndFrame < 0) {
				//rejected
				dtde.rejectDrop();
			}
			//add resource to the channel
			dropCurrentObject();
			dtde.acceptDrop(DnDConstants.ACTION_COPY);
		}

	}
	
	//------------------------------
	// PAINTING
	//------------------------------
	
	/**
	 * Paints a timeline object
	 * @param g the graphics object
	 * @param obj the timeline object
	 * @param channel the channel that contains (or will contain) the resource
	 * @param startFrames the start position of the resource in frames
	 */
	private void paintTimelineObject(Graphics2D g, ResourceTimelineObject<?> obj, int channel) {
		//get frames
		int startInFrames = obj.getStart().toFrames();
		int offsetInFrames = obj.getOffset().toFrames();
		int durationInFrames = obj.getDuration().toFrames();
		//compute sizes
		int startX = (int) (startInFrames * pixelsPerFrame);
		int beginX = (int) ((startInFrames + offsetInFrames) * pixelsPerFrame);
		int beginY = channel * rowHeight;
		int width = (int) ((durationInFrames - startInFrames) * pixelsPerFrame);
		int height = rowHeight;
		//test if visible at all
		Rectangle clip = getVisibleRect();//g.getClipBounds();
		if ((beginX > clip.x + clip.width)
				|| (beginX + width < clip.x)
				|| beginY > clip.y + clip.height
				|| beginY + height < clip.y) {
			return;
		}
		//draw background
		g.setColor(new Color(200, 200, 200));
		g.fillRect(beginX, beginY, width, height);
		
		//draw foreground
		TimelineChannel.DisplayState displayState = project.getTimelineChannels().get(channel).getDisplayState();
		switch (displayState) {
			case TEXT:
				//do nothing, text is always drawn.
				break;
			case VIDEO: if (obj.getResource() instanceof Resource.ImageProvider) {
				Resource.ImageProvider res = (Resource.ImageProvider) obj.getResource();
				Graphics g2 = g.create(beginX + PREVIEW_INTENT, beginY + PREVIEW_INTENT, width - 2*PREVIEW_INTENT, height - 2*PREVIEW_INTENT);
				int imgHeight = height - 2*PREVIEW_INTENT;
				int imgWidth = imgHeight * res.getWidth() / res.getHeight();
				float widthInFrames = imgWidth / pixelsPerFrame;
				Rectangle clip2 = g2.getClipBounds();
				
				for (int i=0; ; ++i) { //i: index of the preview frame (in steps of widthInFrames)
					int frame = (int) (i * widthInFrames);
					if (i * imgWidth - startX > clip2.x + clip2.width) {
						break;
					}
					if ((i+1) * imgWidth - startX < clip2.x) {
						continue;
					}
					frame = obj.convertFrame(Math.max(0, frame));
					Image preview = timelinePreviewFactory.getFramePreview(res, frame);
					if (preview != null) {
						g2.drawImage(preview, i * imgWidth - startX, 0, this);
					}
				}
				g2.dispose();
			} break;
			case AUDIO: if (obj.getResource() instanceof Resource.AudioProvider) {
				Resource.AudioProvider res = (Resource.AudioProvider) obj.getResource();
				Graphics g2 = g.create(beginX + PREVIEW_INTENT, beginY + PREVIEW_INTENT, width - 2*PREVIEW_INTENT, height - 2*PREVIEW_INTENT);
				int imgWidth = timelinePreviewFactory.getWaveformWidth();
				float widthInFrames = imgWidth / pixelsPerFrame;
				Rectangle clip2 = g2.getClipBounds();
				for (int i=0; ; ++i) { //i: index of the preview frame (in steps of widthInFrames)
					int frame = (int) (i * widthInFrames);
					if (i * imgWidth - startX > clip2.x + clip2.width) {
						break;
					}
					if ((i+1) * imgWidth - startX < clip2.x) {
						continue;
					}
					frame = obj.convertFrame(Math.max(0, frame));
					Image preview = timelinePreviewFactory.getWaveformPreview(res, frame, pixelsPerFrame);
					if (preview != null) {
						g2.drawImage(preview, i * imgWidth - startX, 0, this);
					}
				}
				g2.dispose();
			} break;
		}
		{
			//draw text
			int textX = Math.max(beginX, clip.x);
			Graphics g2 = g.create(textX + 5, beginY + 5, width - 20 - textX+beginX, height - 5);
			g2.setFont(getFont());
			g2.setColor(Color.BLACK);
			g2.drawString(obj.getResource().getName(), 0, height - 10);
			g2.dispose();
		}
		{
			//draw filters
			Filter[] fx = obj.getFilterArray();
			if (fx.length > 0) {
				int filterX = Math.max(beginX, clip.x);
				Graphics g2 = g.create(filterX + 5, beginY + 5, width - 10 - filterX + beginX, 24);
				for (int i=0; i<fx.length; ++i) {
					Image icon = fx[i].getIcon();
					g2.drawImage(icon, 26 * i, 0, this);
					if (fx[i] == clickedFilter) {
						g2.drawImage(SELECTED_FILTER_BORDER, 26*i, 0, this);
					}
				}
				g2.dispose();
			}
		}
		
		//disabled marker
		if (!obj.isEnabled()) {
			Graphics2D g2 = (Graphics2D) g.create(beginX, beginY, width, height);
			g2.setPaint(new TexturePaint(DEACTIVATED_PATTERN, new Rectangle(DEACTIVATED_PATTERN.getWidth(), DEACTIVATED_PATTERN.getHeight())));
			g2.fillRect(0, 0, width, height);
			g2.dispose();
		}
		
		//draw snap point
		if (selectedSnapPoint != null) {
			for (SnapPoint.Reference ref : selectedSnapPoint.references) {
				if (ref instanceof SnapPoint.TimelineReference) {
					SnapPoint.TimelineReference tref = (SnapPoint.TimelineReference) ref;
					if (tref.obj == obj) {
						g.setColor(Color.YELLOW);
						if (tref.side) {
							//right
							g.drawPolyline(new int[]{
								beginX + width - 16,
								beginX + width - 6,
								beginX + width - 6,
								beginX + width - 16
							}, new int[]{
								beginY + 6,
								beginY + 6,
								beginY + height - 6,
								beginY + height - 6
							}, 4);
						} else {
							//left
							g.drawPolyline(new int[]{
								beginX + 16,
								beginX + 6,
								beginX + 6,
								beginX + 16
							}, new int[]{
								beginY + 6,
								beginY + 6,
								beginY + height - 6,
								beginY + height - 6
							}, 4);
						}
					}
				}
			}
		}
		
		//draw border
		g.setColor(Color.GRAY);
		g.drawRect(beginX, beginY, width, height);
		if (clickedResource == obj) {
			g.setColor(Color.RED);
			g.drawRect(beginX+2, beginY+2, width-4, height-4);
		}
		if (selectedResource == obj) {
			g.setColor(Color.BLACK);
			g.drawRect(beginX+1, beginY+1, width-2, height-2);
		}
	}
	
	@Override
	public void paint(Graphics g) {
		Rectangle bounds = g.getClipBounds();
		g.setColor(BACKGROUND_COLOR);
		g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
		
		//draw lines
		//row seperators
		g.setColor(rowHeader.FOREGROUND_COLOR);
		for (int i=0; i<project.getTimelineChannels().size(); ++i) {
			TimelineChannel c = project.getTimelineChannels().get(i);
			g.drawLine(bounds.x, (i+1) * rowHeight, bounds.x+bounds.width, (i+1) * rowHeight);
		}
		//column sperators
		int spacingInFrames = computeSpacingInFrames();
		int startIndex = (int) Math.floor(bounds.x / (spacingInFrames * pixelsPerFrame));
		int endIndex = (int) Math.ceil((bounds.x + bounds.width) / (spacingInFrames * pixelsPerFrame));
		g.setColor(rowHeader.FOREGROUND_COLOR);
		for (int j=startIndex; j<=endIndex; ++j) {
			int frame = j * spacingInFrames;
			int posX = (int) (frame * pixelsPerFrame);
			g.drawLine(posX, bounds.y, posX, bounds.y + bounds.height);
		}
		
		//draw objects
		for (int i=0; i<project.getTimelineChannels().size(); ++i) {
			TimelineChannel c = project.getTimelineChannels().get(i);
			for (ResourceTimelineObject obj : c.getObjects()) {
				paintTimelineObject((Graphics2D) g, obj, i);
			}
		}
		
		//draw dnd
		if (dndResource != null && dndFrame>=0) {
			dndResource.setOffset(FrameTime.fromFrames(dndFrame, project.getFramerate()));
			paintTimelineObject((Graphics2D) g, dndResource, dndChannel);
		}
		
		//draw time handler
		g.setColor(TIME_HANDLER_COLOR);
		int posX = (int) (project.getTime().toFrames() * pixelsPerFrame);
		g.drawLine(posX, 0, posX, getHeight());
	}
	
	/**
	 * The header that displays the channel names
	 */
	private class RowHeader extends JComponent implements MouseListener {
		public final Color BACKGROUND_COLOR = new Color(230, 230, 230);
		public final Color FOREGROUND_COLOR = Color.BLACK;
		
		private Font timeFont;
		private int fontWidth;
		private int fontHeight;
		private int fontOffsetY;

		public RowHeader() {
			addMouseListener(this);
		}
		
		public void update() {
			setPreferredSize(new Dimension(channelNameWidth, computeHeight()));
		}

		@Override
		public void paint(Graphics g) {
			if (timeFont == null) {
				//create font
				for (int i=50; i>=5; --i) {
					timeFont = new Font(Font.MONOSPACED, Font.PLAIN, i);
					if (g.getFontMetrics(timeFont).getAscent()<= headerHeight * 0.6) {
						break;
					}
				}
				fontHeight = g.getFontMetrics(timeFont).getAscent();
				fontOffsetY = (headerHeight + fontHeight) / 2;
				fontWidth = g.getFontMetrics(timeFont).stringWidth("0") + 5;
			}
			g.setFont(timeFont);
			
			Rectangle bounds = g.getClipBounds();
			g.setColor(BACKGROUND_COLOR);
			g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
			
			//draw channels
			for (int i=0; i<project.getTimelineChannels().size(); ++i) {
				TimelineChannel c = project.getTimelineChannels().get(i);
				//draw seperator
				g.setColor(FOREGROUND_COLOR);
				g.drawLine(0, (i+1) * rowHeight, channelNameWidth, (i+1) * rowHeight);
				int offY = i * rowHeight + fontOffsetY;
				//draw index
				g.drawString((i+1)+":", 5, offY);
				//draw channel state
				g.setColor(getStateColor(c));
				g.drawString(getStateChar(c), channelNameWidth - 2 * (fontWidth + 6), offY);
				g.setColor(Color.LIGHT_GRAY);
				g.drawRect(channelNameWidth - 2 * (fontWidth + 6) - 4, offY - fontHeight + 4, fontWidth, fontHeight);
				//draw display state
				g.setColor(getDisplayColor(c));
				g.drawString(getDisplayChar(c), channelNameWidth - (fontWidth + 6), offY);
				g.setColor(Color.LIGHT_GRAY);
				g.drawRect(channelNameWidth - (fontWidth + 6) - 4, offY - fontHeight + 4, fontWidth, fontHeight);
			}
		}
		private String getStateChar(TimelineChannel c) {
			switch (c.getState()) {
				case OFF: return "X";
				case ON: return "O";
				case SOLO: return "S";
				default: throw new IllegalArgumentException("unknown state "+c.getState());
			}
		}
		private Color getStateColor(TimelineChannel c) {
			switch (c.getState()) {
				case OFF: return Color.LIGHT_GRAY;
				case ON: return FOREGROUND_COLOR;
				case SOLO: return Color.RED;
				default: throw new IllegalArgumentException("unknown state "+c.getState());
			}
		}
		private String getDisplayChar(TimelineChannel c) {
			switch (c.getDisplayState()) {
				case TEXT: return "T";
				case AUDIO: return "A";
				case VIDEO: return "V";
				default: throw new IllegalArgumentException("unknown display state "+c.getDisplayState());
			}
		}
		private Color getDisplayColor(TimelineChannel c) {
			return FOREGROUND_COLOR;
		}

		@Override
		public void mouseClicked(MouseEvent me) {
			//check y and get timeline channel
			int channel = me.getY() / rowHeight;
			if (channel < 0 || channel >= project.getTimelineChannels().size()) return;
			
			TimelineChannel c = project.getTimelineChannels().get(channel);
			Selections.getInstance().setFocussedObject(c);
			
			int y = me.getY() - channel * rowHeight;
			y = y - fontOffsetY + fontHeight - 4;
			if (y < 0 || y > fontHeight) return;
			
			//check x
			if (channelNameWidth - 2 * (fontWidth + 6) - 4 <= me.getX()
					&& channelNameWidth - 2 * (fontWidth + 6) - 4 + fontWidth >= me.getX()) {
				//toggle channel state
				c.setState(TimelineChannel.State.values()[(c.getState().ordinal() + 1) % TimelineChannel.State.values().length]);
			}
			else if (channelNameWidth - (fontWidth + 6) - 4 <= me.getX()
					&& channelNameWidth - (fontWidth + 6) - 4 + fontWidth >= me.getX()) {
				//toggle display state
				c.setDisplayState(TimelineChannel.DisplayState.values()[(c.getDisplayState().ordinal() + 1) % TimelineChannel.DisplayState.values().length]);
			} else {
				return;
			}
			
			//repaint
			repaint();
			TimelineDisplay.this.repaint();
		}

		@Override
		public void mousePressed(MouseEvent me) {
			
		}

		@Override
		public void mouseReleased(MouseEvent me) {
			
		}

		@Override
		public void mouseEntered(MouseEvent me) {
			
		}

		@Override
		public void mouseExited(MouseEvent me) {
			
		}
	}
	
	private int computeSpacingInFrames() {
		int spacingInFrames = 0;
		int spacings[] = { //different allowed spacings, from large to small
			project.getFramerate() * 60, //a minute
			project.getFramerate() * 20,
			project.getFramerate() * 10,
			project.getFramerate() * 5,
			project.getFramerate() * 2,
			project.getFramerate(), //a second
			project.getFramerate() / 2,
			project.getFramerate() / 5,
			1 // a single frame
		};
		spacingInFrames = spacings[0];
		for (int spacing : spacings) {
			if (spacing * pixelsPerFrame >= columnHeader.fontWidth) {
				spacingInFrames = spacing;
			}
		}
		return spacingInFrames;
	}
	
	/**
	 * The header that displays the timeline
	 */
	private class ColumnHeader extends JComponent implements MouseListener, MouseMotionListener {
		private final String DUMMY_FRAME_TIME = "00,00:00";
		public final Color BACKGROUND_COLOR = new Color(230, 230, 230);
		public final Color FOREGROUND_COLOR = Color.BLACK;
		public final Color LAYOUT_BACKGROUND = new Color(200, 200, 200);
		public final Color LAYOUT_FOREGROUND = new Color(150, 150, 150);
		public final int LAYOUT_HEIGHT = 6;
		
		private Font timeFont;
		private int fontWidth;
		private int fontOffsetY;
		private int charWidth;
		private int charHeight;
		
		private Marker selectedMarker;
		private int offsetX;
		private FrameTime oldTime;
		
		public ColumnHeader() {
			addMouseListener(this);
			addMouseMotionListener(this);
		}
		
		public void update() {
			setPreferredSize(new Dimension(computeWidth(), headerHeight));
		}

		@Override
		public void paint(Graphics g) {
			if (timeFont == null) {
				//create font
				for (int i=50; i>=5; --i) {
					timeFont = new Font(Font.MONOSPACED, Font.PLAIN, i);
					if (g.getFontMetrics(timeFont).getHeight() <= headerHeight * 0.4) {
						break;
					}
				}
				fontOffsetY = (headerHeight + g.getFontMetrics(timeFont).getAscent()) / 2;
				fontWidth = g.getFontMetrics(timeFont).stringWidth(DUMMY_FRAME_TIME) + 5;
				charWidth = g.getFontMetrics(timeFont).charWidth('W'); //assume W is the widest
				charHeight = g.getFontMetrics(timeFont).getAscent();
			}
			g.setFont(timeFont);
			
			//background
			Rectangle bounds = g.getClipBounds();
			g.setColor(BACKGROUND_COLOR);
			g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
			
			//layouts
			for (Layout l : project.getLayouts()) {
				FrameTime[] interval = l.getInfluencingTime();
				if (interval==null) continue;
				if (interval[0] == null || interval[1] == null) continue;
				int startPixel = (int) (interval[0].toFrames() * pixelsPerFrame) + 1;
				int endPixel = (int) (interval[1].toFrames() * pixelsPerFrame) - 1;
				int widthPixel = endPixel - startPixel;
				if (widthPixel <= 0) continue;
				g.setColor(LAYOUT_BACKGROUND);
				g.fillRect(startPixel, headerHeight-LAYOUT_HEIGHT-1, widthPixel, LAYOUT_HEIGHT);
				g.setColor(LAYOUT_FOREGROUND);
				g.drawRect(startPixel, headerHeight-LAYOUT_HEIGHT-1, widthPixel, LAYOUT_HEIGHT);
			}
			
			//draw timeline
			int spacingInFrames = computeSpacingInFrames();
			//ok, each 'spacingInFrames'-frames, we'd like to draw a bar
			int startIndex = (int) Math.floor(bounds.x / (spacingInFrames * pixelsPerFrame));
			int endIndex = (int) Math.ceil((bounds.x + bounds.width) / (spacingInFrames * pixelsPerFrame));
			g.setColor(FOREGROUND_COLOR);
			FrameTime ft = new FrameTime(project.getFramerate());
			for (int j=startIndex; j<=endIndex; ++j) {
				int frame = j * spacingInFrames;
				int posX = (int) (frame * pixelsPerFrame);
				g.drawLine(posX, 0, posX, headerHeight);
				ft = FrameTime.fromFrames(frame, project.getFramerate());
				g.drawString(ft.toString(), posX + 3, fontOffsetY);
			}
			
			//draw markers
			for (Marker m : project.getMarkers()) {
				int posX = (int) (m.getTime().toFrames() * pixelsPerFrame);
				Color c = m.getColor();
				g.setColor(c);
				g.fillRect(posX - charWidth/2 - 2, headerHeight - charHeight - 4, charWidth + 4, charHeight + 4);
				//draw outline if selected
				if (Selections.getInstance().getFocussedObject() == m) {
					g.setColor(Color.BLACK);
					g.drawRect(posX - charWidth/2 - 2, headerHeight - charHeight - 4, charWidth + 4, charHeight + 4);
				}
				//draw outline if snap point
				if (selectedSnapPoint != null) {
					for (SnapPoint.Reference ref : selectedSnapPoint.references) {
						if (ref instanceof SnapPoint.MarkerReference && ((SnapPoint.MarkerReference) ref).marker == m) {
							g.setColor(Color.YELLOW);
							g.drawRect(posX - charWidth/2 - 1, headerHeight - charHeight - 3, charWidth + 2, charHeight + 2);
						}
					}
				}
				//draw char
				double brightness = 0.299*c.getRed() + 0.587*c.getGreen() + 0.114*c.getBlue();
				if (brightness > 128) {
					g.setColor(Color.BLACK);
				} else {
					g.setColor(Color.WHITE);
				}
				g.drawString(m.getDisplayedCharacter(), posX - charWidth/2, headerHeight - 2);
			}
			
			//draw time handler
			g.setColor(TIME_HANDLER_COLOR);
			int posX = (int) (project.getTime().toFrames() * pixelsPerFrame);
			g.drawLine(posX, 0, posX, headerHeight);
			g.fillRect(posX-7, 0, 14, 10);
		}

		private void setTime(int mx) {
			mx -= offsetX;
			//convert x-position to frames
			FrameTime time = null;
			selectedSnapPoint = null;
			if (snapping) {
				SnapPoint p = findClosestSnapPointIndex(mx);
				if (p != null) {
					time = p.time;
					selectedSnapPoint = p;
				}
			}
			if (time == null) {
				int f = (int) (mx / pixelsPerFrame);
				f = Math.min(f, project.computeTotalLength().toFrames());
				time = FrameTime.fromFrames(f, project.getFramerate());
			}
			//set time
			if (selectedMarker != null) {
				selectedMarker.setTimeNoUndo(time);
			} else {
				project.setTime(time);
			}
			//repaint
			repaint();
			TimelineDisplay.this.repaint();
		}
		
		@Override
		public void mouseClicked(MouseEvent me) {
			
		}
		
		private Marker pickMarker(int x, int y) {
			for (Marker m : project.getMarkers()) {
				int posX = (int) (m.getTime().toFrames() * pixelsPerFrame);
				if (x >= posX - charWidth/2 - 2 && x <= posX + charWidth/2 + 2
						&& y >= headerHeight - charHeight - 4 && y <= headerHeight) {
					return m;
				}
			}
			return null;
		}

		@Override
		public void mousePressed(MouseEvent me) {
			selectedMarker = null;
			offsetX = 0;
			//pick marker
			selectedMarker = pickMarker(me.getX(), me.getY());
			if (selectedMarker != null) {
				int posX = (int) (selectedMarker.getTime().toFrames() * pixelsPerFrame);
				offsetX = me.getX() - posX;
				Selections.getInstance().setFocussedObject(selectedMarker);
				oldTime = selectedMarker.getTime();
			}
			if (snapping) {
				createSnapPoints(selectedMarker);
			}
			setTime(me.getX());
		}

		@Override
		public void mouseReleased(MouseEvent me) {
			if (selectedMarker != null) {
				selectedMarker.fireTimeChanged(oldTime, selectedMarker.getTime());
			}
			selectedSnapPoint = null;
			selectedMarker = null;
			//repaint
			repaint();
			TimelineDisplay.this.repaint();
		}

		@Override
		public void mouseEntered(MouseEvent me) {
			
		}

		@Override
		public void mouseExited(MouseEvent me) {
			
		}

		@Override
		public void mouseDragged(MouseEvent me) {
			setTime(me.getX());
		}

		@Override
		public void mouseMoved(MouseEvent me) {
			Marker m = pickMarker(me.getX(), me.getY());
			String text = m==null ? null : m.getTooltip();
			if (text == null || text.isEmpty()) {
				setToolTipText(null);
			} else {
				setToolTipText(text);
			}
		}
		
	}
	
	//------------------------------
	// ACTIONS
	//------------------------------
	
	public static class ToggleObjectEnabledAction extends AbstractAction implements PropertyChangeListener {

		public ToggleObjectEnabledAction() {
			super("Enable/Disable Object");
			super.putValue(Action.SHORT_DESCRIPTION, "Toggles the enabled state of the selected timeline object(s)");
			super.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_E);
			super.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
			super.setEnabled(false);
			Selections.getInstance().addPropertyChangeListener(this);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			TimelineObject obj = Selections.getInstance().getSelectedTimelineObject();
			assert (obj != null);
			obj.setEnabled(!obj.isEnabled());
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getSource() == Selections.getInstance() 
					&& evt.getPropertyName().equals(Selections.PROP_SELECTED_TIMELINE_OBJECT)) {
				setEnabled(Selections.getInstance().getSelectedTimelineObject() != null);
			}
		}
		
	}
	
	public static class DeleteObjectAction extends AbstractAction implements PropertyChangeListener {

		public DeleteObjectAction() {
			super("Delete Object");
			super.putValue(Action.SHORT_DESCRIPTION, "Deletes selected timeline object(s) or marker");
			super.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_D);
			super.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
			super.setEnabled(false);
			Selections.getInstance().addPropertyChangeListener(this);
		}
		
		private void deleteTimelineObject(TimelineObject obj) {
			if (obj instanceof ResourceTimelineObject) {
				obj.getChannel().removeTimelineObject((ResourceTimelineObject) obj);
			} else {
				assert (obj.getParent() != null);
				obj.getParent().removeChild(obj);
			}
		}
		
		private void deleteMarker(Marker marker) {
			marker.getProject().removeMarker(marker);
		}
		
		private void deleteFilter(Filter filter) {
			TimelineObject parent = filter.getParent();
			Filter[] fx = parent.getFilterArray();
			parent.getFilterListToModify().remove(filter);
			parent.fireFilterListChanged(fx);
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = Selections.getInstance().getFocussedObject();
			if (obj instanceof TimelineObject) {
				deleteTimelineObject((TimelineObject) obj);
			} else if (obj instanceof Marker) {
				deleteMarker((Marker) obj);
			} else if (obj instanceof Filter) {
				deleteFilter((Filter) obj);
			}
			Selections.getInstance().setFocussedObject(null);
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getSource() == Selections.getInstance() 
					&& evt.getPropertyName().equals(Selections.PROP_FOCUSED_OBJECT)) {
				Object obj = Selections.getInstance().getFocussedObject();
				setEnabled(
						(obj instanceof TimelineObject) ||
						(obj instanceof Marker) ||
						(obj instanceof Filter));
			}
		}
		
	}
	
	public static class SplitObjectAction extends AbstractAction implements PropertyChangeListener {

		private Project project;
		
		public SplitObjectAction() {
			super("Split object");
			super.putValue(Action.SHORT_DESCRIPTION, "Splits the selected object at the current time");
			super.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
			super.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK));
			super.setEnabled(false);
			Selections.getInstance().addPropertyChangeListener(this);
		}

		public void setProject(Project project) {
			this.project = project;
		}
		
		public static ResourceTimelineObject split(Project project, ResourceTimelineObject obj) {
			FrameTime time = project.getTime();
			FrameTime start = obj.getStart();
			FrameTime offset = obj.getOffset();
			FrameTime duration = obj.getDuration();
			//check if time is within the bounds
			if (time.compareTo(start.add(offset)) < 0) return null;
			if (time.compareTo(duration.add(offset)) >= 0) return null;
			//create resource clones
			ResourceTimelineObject obj1 = obj.clone();
			ResourceTimelineObject obj2 = obj.clone();
			//set times
			obj1.setOffset(offset);
			obj2.setOffset(offset);
			obj1.setStart(start);
			obj1.setDuration(time.subtract(offset));
			obj2.setStart(time.subtract(offset));
			obj2.setDuration(duration);
			//delete old and add new object
			//TODO: merge undo/redo events
			TimelineChannel channel = obj.getChannel();
			channel.removeTimelineObject(obj);
			channel.addTimelineObject(obj1);
			channel.addTimelineObject(obj2);
			//done
			LOG.info("timeline object splitted");
			return obj1;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ResourceTimelineObject obj = (ResourceTimelineObject) Selections.getInstance().getSelectedTimelineObject();
			ResourceTimelineObject obj1 = split(project, obj);
			if (obj1 != null) {
				//select first object
				Selections.getInstance().setSelectedTimelineObject(obj1);
				Selections.getInstance().setFocussedObject(obj1);
			}
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getSource() == Selections.getInstance() 
					&& evt.getPropertyName().equals(Selections.PROP_SELECTED_TIMELINE_OBJECT)) {
				setEnabled((Selections.getInstance().getSelectedTimelineObject() != null )
						&& (Selections.getInstance().getSelectedTimelineObject() instanceof ResourceTimelineObject));
			}
		}
		
	}
	
	public static class SplitAllObjectsAction extends AbstractAction {

		private Project project;
		
		public SplitAllObjectsAction() {
			super("Split all videos");
			super.putValue(Action.SHORT_DESCRIPTION, "Splits all video objects at the current time");
			super.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
			super.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));
			super.setEnabled(false);
		}

		public void setProject(Project project) {
			this.project = project;
			setEnabled(true);
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			for (TimelineChannel c : project.getTimelineChannels()) {
				TimelineObject[] objx = c.getObjects().toArray(new TimelineObject[c.getObjects().size()]);
				for (TimelineObject obj : objx) {
					if (obj instanceof ImageTimelineObject) {
						SplitObjectAction.split(project, (ResourceTimelineObject) obj);
					}
				}
			}
			Selections.getInstance().setSelectedTimelineObject(null);
			Selections.getInstance().setFocussedObject(null);
		}
	}
	
	public static class CreateMarkerAction extends AbstractAction {
		private Project project;
		
		public CreateMarkerAction() {
			super("Create Marker");
			super.putValue(Action.SHORT_DESCRIPTION, "Creates a new marker at the current time");
			super.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_M);
			super.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));
			super.setEnabled(false);
		}
		
		public void setProject(Project project) {
			this.project = project;
		}
		
		@Override
		public void actionPerformed(ActionEvent ae) {
			FrameTime time = project.getTime();
			Marker marker = new Marker(time);
			project.addMarker(marker);
			LOG.log(Level.INFO, "marker added at {0}", time);
		}
		
	}
	
	public static class PreviousMarkerAction extends AbstractAction {

		private Project project;
		
		public PreviousMarkerAction() {
			super("Previous Marker");
			super.putValue(Action.SHORT_DESCRIPTION, "Moves the current time to the previous marker");
			super.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, ActionEvent.CTRL_MASK));
			super.setEnabled(false);
		}
		
		public void setProject(Project project) {
			this.project = project;
		}
		
		@Override
		public void actionPerformed(ActionEvent ae) {
			ArrayList<Marker> markers = project.getMarkers();
			Collections.sort(markers);
			FrameTime time = project.getTime();
			int index = -1;
			for (int i=0; i<markers.size(); ++i) {
				FrameTime t = markers.get(i).getTime();
				if (t.compareTo(time)<0) {
					index = i;
				}
			}
			if (index == -1) {
				//set to the beginning
				project.setTime(new FrameTime(project.getFramerate()));
			} else {
				//set to marker
				project.setTime(markers.get(index).getTime());
			}
		}
		
	}
	
	public static class NextMarkerAction extends AbstractAction {

		private Project project;
		
		public NextMarkerAction() {
			super("Next Marker");
			super.putValue(Action.SHORT_DESCRIPTION, "Moves the current time to the next marker");
			super.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, ActionEvent.CTRL_MASK));
			super.setEnabled(false);
		}
		
		public void setProject(Project project) {
			this.project = project;
		}
		
		@Override
		public void actionPerformed(ActionEvent ae) {
			ArrayList<Marker> markers = project.getMarkers();
			Collections.sort(markers);
			FrameTime time = project.getTime();
			int index = -1;
			for (int i=markers.size()-1; i>=0; --i) {
				FrameTime t = markers.get(i).getTime();
				if (t.compareTo(time)>0) {
					index = i;
				}
			}
			if (index == -1) {
				//set to the end
				project.setTime(project.getLength());
			} else {
				//set to marker
				project.setTime(markers.get(index).getTime());
			}
		}
		
	}
}
