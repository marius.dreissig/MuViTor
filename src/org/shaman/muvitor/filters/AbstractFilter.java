/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.filters;

import com.jme3.asset.AssetManager;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.Listenable;
import org.shaman.muvitor.PresetSaveable;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.timeline.TimelineObject;

/**
 * Abstract filter, implements all interface needed for simple editing.
 * @author Sebastian Weiss
 * @param <T> the type of structure containing the settings
 */
public abstract class AbstractFilter<T> implements Filter, Listenable, PresetSaveable<T> {
	
	protected TimelineObject parent;
	protected Player player;
	protected AssetManager assetManager;
	protected UndoableEditSupport undoSupport;
	protected final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

	@Override
	public void initialize(TimelineObject parent, Player player, UndoableEditSupport undoSupport) {
		this.parent = parent;
		this.player = player;
		this.assetManager = player.getAssetManager();
		this.undoSupport = undoSupport;
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	@Override
	public TimelineObject getParent() {
		return parent;
	}

}
