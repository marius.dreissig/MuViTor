/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.filters;

import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;
import com.jme3.texture.Texture2D;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEditSupport;
import org.openide.util.lookup.ServiceProvider;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.PaintableProperties;
import org.shaman.muvitor.ScreenInteractivable;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.timeline.ImageTimelineObject;
import org.shaman.muvitor.resources.Resource;
import org.shaman.muvitor.resources.VideoResource;
import org.shaman.muvitor.timeline.ResourceTimelineObject;
import org.shaman.muvitor.timeline.TimelineObject;
import org.shaman.muvitor.uitools.YCbCrColorPicker;
import org.simpleframework.xml.Element;

/**
 * A simple green screen / chroma key filter.
 * @author Sebastian Weiss
 */
public class GreenScreenImageFilter extends AbstractFilter<GreenScreenImageFilter.Settings> 
		implements PaintableProperties, ScreenInteractivable {
	private static final Logger LOG = Logger.getLogger(GreenScreenImageFilter.class.getName());
	
	private static final String MATERIAL = "org/shaman/muvitor/filters/GreenScreen.j3md";
	private static final Image ICON;
	static {
		try {
			ICON = ImageIO.read(CrossHatchFilter.class.getResource("greenscreen_icon.png"));
		} catch (IOException ex) {
			throw new Error("unable to load icon", ex);
		}
	}
	private static final String EXTRA_MASK_FILE = "mask.dat";

	private Material material;
	private Material smoothingMat;
	private Material smoothDespillMat;
	private ArrayList<PreFilterPass> passes;
	private PreFilterPass passA;
	private PreFilterPass passB;
	private PreFilterPass passC;
	private Vector4f maskSettingsVector = new Vector4f();
	
	public static class Settings implements Cloneable{
		@Element public float keyCb;
		@Element public float keyCr;
		@Element public float tolA;
		@Element public float tolB;
		@Element public boolean despill;
		@Element public int smoothingSteps;
		@Element(required = false) public boolean useExtraMask;

		@Override
		public Settings clone(){
			try {
				return (Settings) super.clone();
			} catch (CloneNotSupportedException ex) {
				throw new RuntimeException(ex);
			}
		}

		@Override
		public int hashCode() {
			int hash = 3;
			hash = 97 * hash + Float.floatToIntBits(this.keyCb);
			hash = 97 * hash + Float.floatToIntBits(this.keyCr);
			hash = 97 * hash + Float.floatToIntBits(this.tolA);
			hash = 97 * hash + Float.floatToIntBits(this.tolB);
			hash = 97 * hash + (this.despill ? 1 : 0);
			hash = 97 * hash + this.smoothingSteps;
			hash = 97 * hash + (this.useExtraMask ? 1 : 0);
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final Settings other = (Settings) obj;
			if (Float.floatToIntBits(this.keyCb) != Float.floatToIntBits(other.keyCb)) {
				return false;
			}
			if (Float.floatToIntBits(this.keyCr) != Float.floatToIntBits(other.keyCr)) {
				return false;
			}
			if (Float.floatToIntBits(this.tolA) != Float.floatToIntBits(other.tolA)) {
				return false;
			}
			if (Float.floatToIntBits(this.tolB) != Float.floatToIntBits(other.tolB)) {
				return false;
			}
			if (this.despill != other.despill) {
				return false;
			}
			if (this.smoothingSteps != other.smoothingSteps) {
				return false;
			}
			if (this.useExtraMask != other.useExtraMask) {
				return false;
			}
			return true;
		}
		
	}
	
	@Element
	private Settings settings;
	public static final String PROP_SETTINGS = "settings";
	public static final String PROP_KEYCB = "keyCb";
	public static final String PROP_KEYCR = "keyCr";
	public static final String PROP_KEYCOLOR = "keyColor";
	public static final String PROP_TOL_A = "tolA";
	public static final String PROP_TOL_B = "tolB";
	public static final String PROP_DESPILL = "despill";
	public static final String PROP_SMOOTHING = "smoothing";
	public static final String PROP_USE_EXTRA_MASK = "useExtraMask";
	
	private JPanel customUI;
	private YCbCrColorPicker colorPicker;
	private JSlider tolASpinner;
	private JSlider tolBSpinner;
	private JCheckBox despillCheckbox;
	private JSpinner smoothingStepSpinner;
	private JCheckBox useExtraMaskCheckbox;
	private JButton resetExtraMaskButton;
	
	//extra mask
	private ImageTimelineObject<VideoResource> timelineObject;
	private VideoResource resource;
	private static final WeakHashMap<VideoResource, ByteBuffer> EXTRA_MASKS
			= new WeakHashMap<VideoResource, ByteBuffer>();
	private ByteBuffer extraMask;
	private float currentMaskX = -1;
	private float currentMaskY = -1;
	private float currentMaskRadius = 0.05f;
	private boolean maskClicked;
	private int currentFrame;
	
	/**
	 * Serialization only
	 */
	public GreenScreenImageFilter() {
	}
	
	/**
	 * Constructs a new green screen filter
	 * @param keyCb
	 * @param keyCr
	 * @param tolA
	 * @param tolB
	 * @param despill 
	 */
	public GreenScreenImageFilter(float keyCb, float keyCr, float tolA, float tolB, boolean despill) {
		settings = new Settings();
		setKeyColor(keyCb, keyCr);
		setTolA(tolA);
		setTolB(tolB);
		setDespill(despill);
	}

	public GreenScreenImageFilter(Settings settings) {
		this.settings = settings;
	}
	
	
	@Override
	public void initialize(TimelineObject parent, Player player, UndoableEditSupport undoSupport) {
		super.initialize(parent, player, undoSupport);
		setTimelineObject(parent);
		
		this.material = new Material(assetManager, MATERIAL);
		this.smoothingMat = new Material(assetManager, MATERIAL);
		this.smoothDespillMat = new Material(assetManager, MATERIAL);
		this.material.setBoolean("SmoothAlpha", false);
		this.smoothingMat.setBoolean("SmoothAlpha", true);
		this.smoothDespillMat.setBoolean("SmoothAlpha", true);
		this.smoothDespillMat.setBoolean("Despill", true);
		updateMaterial0();
		this.material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Off);
		this.smoothingMat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Off);
		this.smoothDespillMat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Off);
		
		this.passes = new ArrayList<>(1);
		passA = new PreFilterPass() {
			@Override
			public Material getMaterial(Texture2D input) {
				material.setTexture("Texture", input);
				return material;
			}
		};
		passB = new PreFilterPass() {
			@Override
			public Material getMaterial(Texture2D input) {
				smoothingMat.setVector2("PixelSize", new Vector2f(1.0f/input.getImage().getWidth(), 1.0f/input.getImage().getHeight()));
				smoothingMat.setTexture("Texture", input);
				return smoothingMat;
			}
		};
		passC = new PreFilterPass() {
			@Override
			public Material getMaterial(Texture2D input) {
				smoothDespillMat.setVector2("PixelSize", new Vector2f(1.0f/input.getImage().getWidth(), 1.0f/input.getImage().getHeight()));
				smoothDespillMat.setTexture("Texture", input);
				return smoothDespillMat;
			}
		};
	}

	private void updateMaterial() {
		if (player != null) {
			player.enqueue(this::updateMaterial0);
		}
	}
	private void updateMaterial0() {
		material.setFloat("KeyCb", settings.keyCb);
		material.setFloat("KeyCr", settings.keyCr);
		material.setFloat("TolA", settings.tolA);
		material.setFloat("TolB", settings.tolA + settings.tolB);
		material.setBoolean("Despill", settings.despill && settings.smoothingSteps>0);
		material.setBoolean("UseExtraMask", settings.useExtraMask);
		if (settings.useExtraMask) {
			loadExtraMaskData();
			float storedX = extraMask.getFloat(12 * currentFrame);
			float storedY = extraMask.getFloat(12 * currentFrame + 4);
			float storedR = extraMask.getFloat(12 * currentFrame + 8);
			maskSettingsVector.x = storedX;
			maskSettingsVector.y = 1-storedY;
			if (storedR > 0) {
				maskSettingsVector.z = storedR * storedR;
				maskSettingsVector.w = (storedR * timelineObject.getAspect()) * (storedR * timelineObject.getAspect());
			} else {
				maskSettingsVector.z = maskSettingsVector.w = -1;
			}
			material.setVector4("Mask", maskSettingsVector);
		}
		smoothingMat.setBoolean("Despill", settings.despill);
		smoothDespillMat.setFloat("KeyCb", settings.keyCb);
		smoothDespillMat.setFloat("KeyCr", settings.keyCr);
		smoothDespillMat.setFloat("TolA", settings.tolA);
		smoothDespillMat.setFloat("TolB", settings.tolA + settings.tolB);
	}

	private void fireColorEvent(float oldCb, float oldCr, float newCb, float newCr) {
		propertyChangeSupport.firePropertyChange(PROP_KEYCOLOR, new float[]{oldCb, oldCr}, new float[]{newCb, newCr});
		if (undoSupport!=null && oldCb!=newCb && oldCr!=newCr) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.keyCb = oldCb;
					settings.keyCr = oldCr;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_KEYCOLOR, new float[]{newCb, newCr}, new float[]{oldCb, oldCr});
					if (colorPicker != null) {
						colorPicker.setCurrentCb(settings.keyCb);
						colorPicker.setCurrentCr(settings.keyCr);
					}
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.keyCb = newCb;
					settings.keyCr = newCr;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_KEYCOLOR, new float[]{oldCb, oldCr}, new float[]{newCb, newCr});
					if (colorPicker != null) {
						colorPicker.setCurrentCb(settings.keyCb);
						colorPicker.setCurrentCr(settings.keyCr);
					}
				}
				
			});
		}
	}
	
	public void setKeyColor(float keyCb, float keyCr) {
		settings.keyCb = keyCb;
		settings.keyCr = keyCr;
	}
	
	public float getKeyCb() {
		return settings.keyCb;
	}

	public void setKeyCb(float keyCb) {
		final float oldKeyCb = keyCb;
		settings.keyCb = keyCb;
		updateMaterial();
		fireColorEvent(oldKeyCb, settings.keyCr, keyCb, settings.keyCr);
	}
	
	public float getKeyCr() {
		return settings.keyCr;
	}

	public void setKeyCr(float keyCr) {
		final float oldKeyCr = keyCr;
		settings.keyCr = keyCr;
		updateMaterial();
		fireColorEvent(settings.keyCb, oldKeyCr, settings.keyCb, keyCr);
	}

	/**
	 * Get the value of tolA
	 *
	 * @return the value of tolA
	 */
	public float getTolA() {
		return settings.tolA;
	}

	/**
	 * Set the value of tolA
	 *
	 * @param newTolA new value of tolA
	 */
	public void setTolA(final float newTolA) {
		final float oldTolA = settings.tolA;
		settings.tolA = newTolA;
		updateMaterial();
		propertyChangeSupport.firePropertyChange(PROP_TOL_A, oldTolA, newTolA);
		if (!Objects.equals(oldTolA, newTolA) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.tolA = oldTolA;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_TOL_A, newTolA, oldTolA);
					if (customUI != null) {
						colorPicker.setTolerance(settings.tolA, settings.tolA + settings.tolB);
						tolASpinner.setValue((int) (settings.tolA * 1000));
					}
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.tolA = newTolA;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_TOL_A, oldTolA, newTolA);
					if (customUI != null) {
						colorPicker.setTolerance(settings.tolA, settings.tolA + settings.tolB);
						tolASpinner.setValue((int) (settings.tolA * 1000));
					}
				}
			
			});
		}
	}

	/**
	 * Get the value of tolB
	 *
	 * @return the value of tolB
	 */
	public float getTolB() {
		return settings.tolB;
	}

	/**
	 * Set the value of tolB
	 *
	 * @param newTolB new value of tolB
	 */
	public void setTolB(final float newTolB) {
		final float oldTolB = settings.tolB;
		settings.tolB = newTolB;
		updateMaterial();
		propertyChangeSupport.firePropertyChange(PROP_TOL_B, oldTolB, newTolB);
		if (!Objects.equals(oldTolB, newTolB) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.tolB = oldTolB;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_TOL_B, newTolB, oldTolB);
					if (customUI != null) {
						colorPicker.setTolerance(settings.tolA, settings.tolA + settings.tolB);
						tolBSpinner.setValue((int) (settings.tolB * 1000));
					}
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.tolB = newTolB;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_TOL_B, oldTolB, newTolB);
					if (customUI != null) {
						colorPicker.setTolerance(settings.tolA, settings.tolA + settings.tolB);
						tolBSpinner.setValue((int) (settings.tolB * 1000));
					}
				}
			
			});
		}
	}

	public boolean isDespill() {
		return settings.despill;
	}

	public void setDespill(boolean newDespill) {
		boolean oldDespill = settings.despill;
		settings.despill = newDespill;
		updateMaterial();
		propertyChangeSupport.firePropertyChange(PROP_DESPILL, oldDespill, newDespill);
		if (!Objects.equals(oldDespill, newDespill) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.despill = oldDespill;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_DESPILL, newDespill, oldDespill);
					if (despillCheckbox != null) {
						despillCheckbox.setSelected(settings.despill);
					}
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.despill = newDespill;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_DESPILL, oldDespill, newDespill);
					if (despillCheckbox != null) {
						despillCheckbox.setSelected(settings.despill);
					}
				}
			
			});
		}
	}

	public int getSmoothingSteps() {
		return settings.smoothingSteps;
	}

	public void setSmoothingSteps(int newSmoothingSteps) {
		int oldSmoothingSteps = settings.smoothingSteps;
		settings.smoothingSteps = newSmoothingSteps;
		updateMaterial();
		propertyChangeSupport.firePropertyChange(PROP_SMOOTHING, oldSmoothingSteps, newSmoothingSteps);
		if (!Objects.equals(oldSmoothingSteps, newSmoothingSteps) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.smoothingSteps = oldSmoothingSteps;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_SMOOTHING, newSmoothingSteps, oldSmoothingSteps);
					if (smoothingStepSpinner != null) {
						smoothingStepSpinner.setValue(settings.smoothingSteps);
					}
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.smoothingSteps = newSmoothingSteps;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_SMOOTHING, oldSmoothingSteps, newSmoothingSteps);
					if (smoothingStepSpinner != null) {
						smoothingStepSpinner.setValue(settings.smoothingSteps);
					}
				}
			
			});
		}
	}
	
	public boolean isUseExtraMask() {
		return settings.useExtraMask;
	}

	public void setUseExtraMask(boolean use) {
		if (timelineObject == null) use = false; //can't enable when we have no timeline object
		boolean newUseExtraMask = use;
		boolean oldUseExtraMask = settings.despill;
		settings.useExtraMask = newUseExtraMask;
		updateMaterial();
		propertyChangeSupport.firePropertyChange(PROP_USE_EXTRA_MASK, oldUseExtraMask, newUseExtraMask);
		if (!Objects.equals(oldUseExtraMask, newUseExtraMask) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.useExtraMask = oldUseExtraMask;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_USE_EXTRA_MASK, newUseExtraMask, oldUseExtraMask);
					if (despillCheckbox != null) {
						despillCheckbox.setSelected(settings.despill);
					}
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.useExtraMask = newUseExtraMask;
					updateMaterial();
					propertyChangeSupport.firePropertyChange(PROP_USE_EXTRA_MASK, oldUseExtraMask, newUseExtraMask);
					if (despillCheckbox != null) {
						despillCheckbox.setSelected(settings.despill);
					}
				}
			
			});
		}
	}

	@Override
	public void loadPresetSettings(Settings s) {
		Settings newSettings = s.clone();
		Settings oldSettings = this.settings.clone();
		this.settings = newSettings.clone();
		updateMaterial();
		updateUI();
		propertyChangeSupport.firePropertyChange(PROP_SETTINGS, oldSettings, newSettings);
		if (undoSupport!=null && !Objects.equals(newSettings, oldSettings)) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings = oldSettings.clone();
					propertyChangeSupport.firePropertyChange(PROP_SETTINGS, newSettings, oldSettings);
					updateMaterial();
					updateUI();
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings = newSettings.clone();
					propertyChangeSupport.firePropertyChange(PROP_SETTINGS, oldSettings, newSettings);
					updateMaterial();
					updateUI();
				}
			
			});
		}
	}

	@Override
	public Settings savePresetSettings() {
		return settings.clone();
	}

	@Override
	public Class<?> presetClass() {
		return Settings.class;
	}
	
	@Override
	public Filter cloneForParent(TimelineObject parent) {
		GreenScreenImageFilter f = new GreenScreenImageFilter(settings.clone());
		f.setTimelineObject(parent);
		return f;
	}

	@Override
	public List<? extends PreFilterPass> getPreFilterPasses(FrameTime time) {
		//extra mask
		if (timelineObject != null) {
			currentFrame = time.toFrames() - timelineObject.getOffset().toFrames();
			//store current selection per frame
			if (maskClicked) {
				loadExtraMaskData();
				float x, y, r;
				if (currentMaskX>=0 && currentMaskX<=1 && currentMaskY>=0 && currentMaskY<=1) {
					x = currentMaskX;
					y = currentMaskY;
					r = currentMaskRadius;
				} else {
					x = -1;
					y = -1;
					r = -1;
				}
				extraMask.putFloat(12 * currentFrame, x);
				extraMask.putFloat(12 * currentFrame + 4, y);
				extraMask.putFloat(12 * currentFrame + 8, r);
			}
			//send to material
			if (isUseExtraMask()) {
				updateMaterial0();
			}
		}
		
		//process filter passes
		passes.clear();
		passes.add(passA);
		for (int i=1; i<settings.smoothingSteps; ++i)
			passes.add(passB);
		if (settings.despill && settings.smoothingSteps>0)
			passes.add(passC);
		else if (!settings.despill && settings.smoothingSteps>0)
			passes.add(passC);
		return passes;
	}

	@Override
	public Image getIcon() {
		return ICON;
	}

	@Override
	public String toString() {
		return "Green Screen Filter";
	}
	
	@Override
	public JComponent getPropertyComponent() {
		if (customUI == null) {
			//create it
			customUI = new JPanel();
			colorPicker = new YCbCrColorPicker();
			tolASpinner = new JSlider(0, 1000);
			tolBSpinner = new JSlider(0, 1000);
			despillCheckbox = new JCheckBox("despill");
			smoothingStepSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 50, 1));
			useExtraMaskCheckbox = new JCheckBox("use extra mask");
			resetExtraMaskButton = new JButton("reset extra mask");
			UIListener l = new UIListener();
			colorPicker.addPropertyChangeListener(l);
			tolASpinner.addChangeListener(l);
			tolBSpinner.addChangeListener(l);
			despillCheckbox.addChangeListener(l);
			smoothingStepSpinner.addChangeListener(l);
			useExtraMaskCheckbox.addChangeListener(l);
			resetExtraMaskButton.addActionListener(l);
			
			//layout
			customUI.setLayout(new BoxLayout(customUI, BoxLayout.Y_AXIS));
			customUI.add(new JLabel("key color"));
			customUI.add(colorPicker);
			customUI.add(Box.createVerticalStrut(10));
			customUI.add(new JLabel("threshold"));
			customUI.add(tolASpinner);
			customUI.add(Box.createVerticalStrut(5));
			customUI.add(new JLabel("tolerance"));
			customUI.add(tolBSpinner);
			customUI.add(Box.createVerticalStrut(5));
			customUI.add(despillCheckbox);
			customUI.add(Box.createVerticalStrut(10));
			JPanel p = new JPanel();
			p.setLayout(new FlowLayout());
			p.add(new JLabel("smoothing steps: "));
			smoothingStepSpinner.setPreferredSize(new Dimension(100, smoothingStepSpinner.getPreferredSize().height));
			p.add(smoothingStepSpinner);
			p.setAlignmentX(JPanel.LEFT_ALIGNMENT);
			customUI.add(p);
			customUI.add(Box.createVerticalStrut(10));
			customUI.add(useExtraMaskCheckbox);
			customUI.add(Box.createVerticalStrut(5));
			customUI.add(resetExtraMaskButton);
			customUI.add(Box.createVerticalGlue());
		}
		
		//set the current values
		updateUI();
		
		//return it
		return customUI;
	}
	private void updateUI() {
		colorPicker.setCurrentCb(settings.keyCb);
		colorPicker.setCurrentCr(settings.keyCr);
		colorPicker.setTolerance(settings.tolA, settings.tolA + settings.tolB);
		tolASpinner.setValue((int) (settings.tolA * 1000));
		tolBSpinner.setValue((int) (settings.tolB * 1000));
		despillCheckbox.setSelected(settings.despill);
		smoothingStepSpinner.setValue(settings.smoothingSteps);
	}
	private class UIListener implements PropertyChangeListener, ChangeListener, ActionListener {

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getSource() == colorPicker
					&& (evt.getPropertyName()==YCbCrColorPicker.PROP_COLOR_CHANGED
					|| evt.getPropertyName()==YCbCrColorPicker.PROP_COLOR_DRAGGING)) {
				float[] newValue = (float[]) evt.getNewValue();
				settings.keyCb = newValue[0];
				settings.keyCr = newValue[1];
				updateMaterial();
				if (evt.getPropertyName()==YCbCrColorPicker.PROP_COLOR_CHANGED) {
					float[] oldValue = (float[]) evt.getOldValue();
					fireColorEvent(oldValue[0], oldValue[1], newValue[0], newValue[1]);
				}
			}
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			if (e.getSource() == tolASpinner) {
				setTolA(tolASpinner.getValue() / 1000f);
				colorPicker.setTolerance(settings.tolA, settings.tolA + settings.tolB);
			} else if (e.getSource() == tolBSpinner) {
				setTolB(tolBSpinner.getValue()/1000f);
				colorPicker.setTolerance(settings.tolA, settings.tolA + settings.tolB);
			} else if (e.getSource() == despillCheckbox) {
				setDespill(despillCheckbox.isSelected());
			} else if (e.getSource() == smoothingStepSpinner) {
				setSmoothingSteps((int) smoothingStepSpinner.getValue());
			} else if (e.getSource() == useExtraMaskCheckbox) {
				setUseExtraMask(useExtraMaskCheckbox.isSelected());
			}
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == resetExtraMaskButton) {
				resetExtraMaskData();
			}
		}
		
	}

	
	// Support for extra masks
	@SuppressWarnings("unchecked")
	private GreenScreenImageFilter setTimelineObject(TimelineObject o) {
		assert (o instanceof ImageTimelineObject);
		Resource res = ((ResourceTimelineObject) o).getResource();
		if (res instanceof VideoResource) {
			timelineObject = (ImageTimelineObject<VideoResource>) o;
			resource = (VideoResource) res;
		} else {
			timelineObject = null;
			resource = null;
		}
		return this;
	}
	
	private void loadExtraMaskData() {
		if (extraMask != null) {
			return;
		}
		Objects.requireNonNull(resource);
		synchronized(EXTRA_MASKS) {
			extraMask = EXTRA_MASKS.get(resource);
			//try to load
			if (extraMask == null) {
				File f = new File(resource.getStorageFolder(), EXTRA_MASK_FILE);
				if (f.exists()) {
					extraMask = ByteBuffer.allocate(4 * 3 * resource.getDurationInFrames());
					try (FileInputStream in = new FileInputStream(f)) {
						byte[] array = extraMask.array();
						int remaining = array.length;
						while (remaining > 0) {
							remaining -= in.read(array, array.length-remaining, remaining);
						}
					} catch (IOException ex) {
						Logger.getLogger(GreenScreenImageFilter.class.getName()).log(Level.SEVERE, "unable to load mask", ex);
						extraMask = null;
					}
				}
			}
			//create new
			if (extraMask == null) {
				extraMask = ByteBuffer.allocate(4 * 3 * resource.getDurationInFrames());
				EXTRA_MASKS.put(resource, extraMask);
			}
		}
	}
	
	private void saveExtraMaskData() {
		if (extraMask == null || resource == null) {
			return;
		}
		File f = new File(resource.getStorageFolder(), EXTRA_MASK_FILE);
		try (FileOutputStream out = new FileOutputStream(f)) {
			byte[] array = extraMask.array();
			out.write(array);
		} catch (IOException ex) {
			LOG.log(Level.SEVERE, "unable to save mask", ex);
			return;
		}
		LOG.info("extra mask saved");
	}
	
	private void resetExtraMaskData() {
		if (extraMask == null || resource == null) {
			return;
		}
		File f = new File(resource.getStorageFolder(), EXTRA_MASK_FILE);
		f.delete();
		EXTRA_MASKS.remove(resource);
		extraMask = null;
	}
	
	@Override
	public void drawOnScreen(Graphics2D g) {
		//current selection
		if (currentMaskX>=0 && currentMaskX<=1 && currentMaskY>=0 && currentMaskY<=1) {
			g.setColor(Color.RED);
			g.drawOval(
					convertBackX(currentMaskX)-convertRadiusX(currentMaskRadius),
					convertBackY(currentMaskY)-convertRadiusY(currentMaskRadius), 
					2*convertRadiusX(currentMaskRadius),
					2*convertRadiusY(currentMaskRadius));
		}
		//stored mask
		loadExtraMaskData();
		float storedX = extraMask.getFloat(12 * currentFrame);
		float storedY = extraMask.getFloat(12 * currentFrame + 4);
		float storedR = extraMask.getFloat(12 * currentFrame + 8);
		if (storedX>=0 && storedX<=1 && storedY>=0 && storedY<=1 && storedR>0) {
			g.setColor(Color.BLUE);
			g.drawOval(
					convertBackX(storedX)-convertRadiusX(storedR),
					convertBackY(storedY)-convertRadiusY(storedR), 
					2*convertRadiusX(storedR),
					2*convertRadiusY(storedR));
		}
	}

	private int convertRadiusX(float radius) {
		return (int) (radius * timelineObject.getWidth());
	}
	private int convertRadiusY(float radius) {
		return (int) (radius * timelineObject.getWidth());
	}
	private int convertBackX(float x) {
		return (int) ((x * timelineObject.getWidth()) + timelineObject.getX());
	}
	private int convertBackY(float y) {
		return (int) ((y * timelineObject.getHeight()) + timelineObject.getY());
	}
	private void convertMousePosition(MouseEvent e) {
		float x = e.getX();
		float y = e.getY();
		x -= timelineObject.getX();
		y -= timelineObject.getY();
		x /= timelineObject.getWidth();
		y /= timelineObject.getHeight();
		currentMaskX = x;
		currentMaskY = y;
//		System.out.println("sx="+e.getXOnScreen()+", sy="+e.getYOnScreen()+
//				", ox="+e.getX()+", oy="+e.getY()+
//				", mx="+x+", my="+y);
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		currentMaskRadius += e.getPreciseWheelRotation() * 0.01f;
		currentMaskRadius = Math.max(0, currentMaskRadius);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		maskClicked = true;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		maskClicked = false;
		//store extra mask
		saveExtraMaskData();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		maskClicked = false;
		saveExtraMaskData();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		convertMousePosition(e);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		convertMousePosition(e);
	}
	
	
	// FACTORY
	
	@ServiceProvider(service = FilterFactory.class)
	public static class Factory implements FilterFactory {

		@Override
		public String getName() {
			return "Image/Green Screen";
		}

		@Override
		public boolean isApplicable(ResourceTimelineObject<Resource> obj) {
			return obj instanceof ImageTimelineObject;
		}

		@Override
		public Filter createFilter(ResourceTimelineObject<Resource> obj) {
			return new GreenScreenImageFilter(-0.45f, -0.45f, 0.4f, 0.2f, true)
					.setTimelineObject(obj);
		}

		@Override
		public Image getIcon() {
			return ICON;
		}
		
	}
}
