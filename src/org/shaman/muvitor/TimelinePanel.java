/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

import java.awt.event.KeyEvent;
import org.shaman.muvitor.timeline.ResourceTimelineObject;
import org.shaman.muvitor.timeline.TimelineObject;
import org.shaman.muvitor.resources.Resource;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.shaman.muvitor.player.Player;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEditSupport;
import org.openide.util.Lookup;
import org.shaman.muvitor.filters.Filter;
import org.shaman.muvitor.filters.FilterFactory;

/**
 *
 * @author Sebastian
 */
public class TimelinePanel extends javax.swing.JPanel 
		implements PropertyChangeListener, TimelineDisplay.ContextMenuListener {
	private static final Logger LOG = Logger.getLogger(TimelinePanel.class.getName());

	private Project project;
	private UndoableEditSupport undoSupport;
	private Player player;
	
	private TimelineDisplay timelineDisplay;
	private Filter selectedFilter;
	
	//actions
	TimelineDisplay.ToggleObjectEnabledAction toggleObjectEnabledAction;
	TimelineDisplay.DeleteObjectAction deleteObjectAction;
	TimelineDisplay.SplitObjectAction splitObjectAction;
	TimelineDisplay.SplitAllObjectsAction splitAllObjectsAction;
	Action zoomInAction = new AbstractAction("Zoom In", new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/zoom_in16.png"))) {
		@Override
		public void actionPerformed(ActionEvent ae) {
			zoomInButtonEvent(ae);
		}
	};
	Action zoomOutAction = new AbstractAction("Zoom Out", new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/zoom_out16.png"))) {
		@Override
		public void actionPerformed(ActionEvent ae) {
			zoomOutButtonEvent(ae);
		}
	};
	Action snapAction = new AbstractAction("Snap", new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/snap16.png"))) {
		@Override
		public void actionPerformed(ActionEvent ae) {
			boolean selected = (boolean) getValue(Action.SELECTED_KEY);
			timelineDisplay.setSnapping(selected);
			LOG.info("change snap state to "+selected);
		}
	};
	TimelineDisplay.CreateMarkerAction createMarkerAction;
	TimelineDisplay.PreviousMarkerAction previousMarkerAction;
	TimelineDisplay.NextMarkerAction nextMarkerAction;
	
	/**
	 * Creates new form TimelinePanel
	 */
	public TimelinePanel() {
		//create additional actions
		toggleObjectEnabledAction = new TimelineDisplay.ToggleObjectEnabledAction();
		deleteObjectAction = new TimelineDisplay.DeleteObjectAction();
		splitObjectAction = new TimelineDisplay.SplitObjectAction();
		splitAllObjectsAction = new TimelineDisplay.SplitAllObjectsAction();
		zoomInAction.setEnabled(false);
		zoomInAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_ADD, ActionEvent.CTRL_MASK));
		zoomOutAction.setEnabled(false);
		zoomOutAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, ActionEvent.CTRL_MASK));
		snapAction.setEnabled(false);
		snapAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK));
		snapAction.putValue(Action.SELECTED_KEY, false);
		createMarkerAction = new TimelineDisplay.CreateMarkerAction();
		createMarkerAction.setEnabled(false);
		previousMarkerAction = new TimelineDisplay.PreviousMarkerAction();
		previousMarkerAction.setEnabled(false);
		nextMarkerAction = new TimelineDisplay.NextMarkerAction();
		nextMarkerAction.setEnabled(false);
		
		initComponents();
		snapButton.setText("");
	}
	
	public void initMenu(JMenuBar menu, JToolBar toolbar) {		
		//add actions to menu
		JMenu timelineMenu = new JMenu("Timeline");
		timelineMenu.setMnemonic(KeyEvent.VK_T);
		menu.add(timelineMenu);
		
		timelineMenu.add(toggleObjectEnabledAction);
		timelineMenu.add(deleteObjectAction);
		timelineMenu.add(splitObjectAction);
		timelineMenu.add(splitAllObjectsAction);
		timelineMenu.add(new JCheckBoxMenuItem(snapAction));
		
		timelineMenu.addSeparator();
		
		timelineMenu.add(createMarkerAction);
		timelineMenu.add(previousMarkerAction);
		timelineMenu.add(nextMarkerAction);
		
		timelineMenu.addSeparator();
		
		timelineMenu.add(zoomInAction);
		timelineMenu.add(zoomOutAction);
		
		timelineMenu.addSeparator();
		timelineMenu.add(new JCheckBoxMenuItem(MusicVideoEditor.MAIN_FRAME.timelineExtraFrameAction));
	}
	
	public void setProject(Project project) {
		this.project = project;
		project.addPropertyChangeListener(this);
		
		//init timeline
		timelineDisplay = TimelineDisplay.create(project, scrollPane);
		timelineDisplay.setContextMenuListener(this);
		
		//init actions
		splitObjectAction.setProject(project);
		splitAllObjectsAction.setProject(project);
		createMarkerAction.setProject(project);
		previousMarkerAction.setProject(project);
		nextMarkerAction.setProject(project);
		
		addButton.setEnabled(true);
		zoomInButton.setEnabled(true);
		zoomOutButton.setEnabled(true);
		zoomInAction.setEnabled(true);
		zoomOutAction.setEnabled(true);
		snapAction.setEnabled(true);
		createMarkerAction.setEnabled(true);
		previousMarkerAction.setEnabled(true);
		nextMarkerAction.setEnabled(true);
	}

	public void setUndoSupport(UndoableEditSupport undoSupport) {
		this.undoSupport = undoSupport;
	}

	public void setPlayer(Player player) {
		this.player = player;
		timelineDisplay.setPlayer(player);
	}

	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (player == null) return;
		if (pce.getSource() == project) {
			if (player.isRecordFrames()) {
				return;
			}
			switch (pce.getPropertyName()) {
				//TODO: does it need to listen to something?
			}
		}
	}
		
	private void addFilter(final ResourceTimelineObject<Resource> obj, FilterFactory factory) {
		addFilter(obj, factory.createFilter(obj));
	}
	private void addFilter(final ResourceTimelineObject<Resource> obj, Filter filter) {
		filter.initialize(obj, player, undoSupport);
		Filter[] oldContents = obj.getFilterArray();
		obj.getFilterListToModify().add(filter);
		obj.fireFilterListChanged(oldContents);
		LOG.log(Level.INFO, "filter {0} added to {1}", new Object[]{filter, obj});
	}

	@Override
	public void onMenuOpen(Object selectedObj, Point mouse) {
		if (selectedObj instanceof TimelineObject) {
			triggerPopup((TimelineObject) selectedObj, mouse);
		} else if (selectedObj instanceof Filter) {
			triggerPopup((Filter) selectedFilter, mouse);
		}
	}	
	
	private void triggerPopup(Filter filter, Point mouse) {
		selectedFilter = filter;
		LOG.info(filter+" copied");
	}
	
	private void triggerPopup(TimelineObject tobj, Point mouse) {
		LOG.log(Level.INFO, "trigger popup on {0}", tobj);
		//until now, filters can only be applied to resources
		if (tobj instanceof ResourceTimelineObject) {
			@SuppressWarnings("unchecked")
			final ResourceTimelineObject<Resource> obj = (ResourceTimelineObject<Resource>) tobj;
			//test which filters can be applied
			ArrayList<FilterFactory> factories = new ArrayList<>();
			for (FilterFactory f : Lookup.getDefault().lookupAll(FilterFactory.class)) {
				if (f.isApplicable(obj)) {
					factories.add(f);
				}
			}
			Collections.sort(factories, new Comparator<FilterFactory>() {
				@Override
				public int compare(FilterFactory o1, FilterFactory o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			//build menu
			JPopupMenu popup = new JPopupMenu();
			if (selectedFilter != null) {
				JMenuItem pasteItem = new JMenuItem("paste: "+selectedFilter.toString());
				pasteItem.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						addFilter(obj, selectedFilter);
					}
				});
				popup.add(pasteItem);
			}
			Map<String, JMenu> items = new HashMap<>();
			for (final FilterFactory f : factories) {
				JMenu parent = null;
				int index = 0;
				while (index != -1) {
					index = f.getName().indexOf('/', index);
					if (index != -1) {
						String prefix = f.getName().substring(0, index);
						index++;
						JMenu item = items.get(prefix);
						if (item == null) {
							item = new JMenu(prefix.substring(prefix.lastIndexOf('/')+1));
							items.put(prefix, item);
							if (parent != null) {
								parent.add(item);
							} else {
								popup.add(item);
							}
						}
						parent = item;
					}
				}
				String name = f.getName().substring(f.getName().lastIndexOf('/')+1);
				JMenuItem item = new JMenuItem(name);
				parent.add(item);
				item.setIcon(new ImageIcon(f.getIcon()));
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						addFilter(obj, f);
					}
				});
			}
			popup.show(timelineDisplay, mouse.x, mouse.y);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addButton = new javax.swing.JButton();
        removeButton = new javax.swing.JButton();
        upButton = new javax.swing.JButton();
        downButton = new javax.swing.JButton();
        scrollPane = new javax.swing.JScrollPane();
        zoomInButton = new javax.swing.JButton();
        zoomOutButton = new javax.swing.JButton();
        snapButton = new javax.swing.JToggleButton();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setMinimumSize(new java.awt.Dimension(400, 100));

        addButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/plus16.png"))); // NOI18N
        addButton.setToolTipText("add selected resource to the timeline");
        addButton.setEnabled(false);
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonEvent(evt);
            }
        });

        removeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/minus16.png"))); // NOI18N
        removeButton.setToolTipText("Remove the selected object");
        removeButton.setEnabled(false);
        removeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeButtonEvent(evt);
            }
        });

        upButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/up16.png"))); // NOI18N
        upButton.setToolTipText("moves the object up");
        upButton.setEnabled(false);
        upButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                upButtonEvent(evt);
            }
        });

        downButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/down16.png"))); // NOI18N
        downButton.setToolTipText("moves the object down");
        downButton.setEnabled(false);
        downButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downButtonEvent(evt);
            }
        });

        scrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        zoomInButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/zoom_in16.png"))); // NOI18N
        zoomInButton.setToolTipText("zoom in");
        zoomInButton.setEnabled(false);
        zoomInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomInButtonEvent(evt);
            }
        });

        zoomOutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/zoom_out16.png"))); // NOI18N
        zoomOutButton.setToolTipText("zoom out");
        zoomOutButton.setEnabled(false);
        zoomOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zoomOutButtonEvent(evt);
            }
        });

        snapButton.setAction(snapAction);
        snapButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/snap16.png"))); // NOI18N
        snapButton.setToolTipText("snap");
        snapButton.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(addButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(removeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(upButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(downButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(zoomInButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(zoomOutButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(snapButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(removeButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(upButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(downButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zoomInButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zoomOutButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(snapButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(scrollPane, javax.swing.GroupLayout.Alignment.TRAILING)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonEvent
        timelineDisplay.addChannel();
    }//GEN-LAST:event_addButtonEvent

    private void removeButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeButtonEvent
        /*
		final TimelineObject obj = selections.getSelectedTimelineObject();
		assert (obj != null);
		if (obj.getParent() == null) {
			project.removeTimelineObject(obj);
		} else {
			obj.getParent().removeChild(obj);
		}
		tableModel.fireUpdate();
		table.clearSelection();
		LOG.log(Level.INFO, "timeline object {0} removed", obj);
		undoSupport.postEdit(new AbstractUndoableEdit() {

			@Override
			public void undo() throws CannotUndoException {
				super.undo();
				if (obj.getParent() == null) {
					project.addTimelineObject(obj);
				} else {
					obj.getParent().addChild(obj);
				}
				tableModel.fireUpdate();
				LOG.info("undo: remove timeline object");
			}

			@Override
			public void redo() throws CannotRedoException {
				super.redo();
				if (obj.getParent() == null) {
					project.removeTimelineObject(obj);
				} else {
					obj.getParent().removeChild(obj);
				}
				tableModel.fireUpdate();
				LOG.info("redo: remove timeline object");
			}
		});
		*/
    }//GEN-LAST:event_removeButtonEvent

	private void move(TimelineObject obj, int dir) {
		/*
		int index = project.getTimelineObjects().indexOf(obj);
		int newIndex = index + dir;
		if (newIndex >= 0 && newIndex < project.getTimelineObjects().size()) {
			project.getTimelineObjects().remove(index);
			project.getTimelineObjects().add(newIndex, obj);
			project.fireTimelineObjectsChanged();
			tableModel.fireUpdate();
		}
		*/
	}
	
    private void upButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_upButtonEvent
        final TimelineObject obj = Selections.getInstance().getSelectedTimelineObject();
		assert (obj != null);
		move(obj, -1);
		undoSupport.postEdit(new AbstractUndoableEdit(){

			@Override
			public void undo() throws CannotUndoException {
				super.undo();
				move(obj, 1);
			}

			@Override
			public void redo() throws CannotRedoException {
				super.redo();
				move(obj, -1);
			}
			
		});
    }//GEN-LAST:event_upButtonEvent

    private void downButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downButtonEvent
        final TimelineObject obj = Selections.getInstance().getSelectedTimelineObject();
		assert (obj != null);
		move(obj, 1);
		undoSupport.postEdit(new AbstractUndoableEdit(){

			@Override
			public void undo() throws CannotUndoException {
				super.undo();
				move(obj, -1);
			}

			@Override
			public void redo() throws CannotRedoException {
				super.redo();
				move(obj, 1);
			}
			
		});
    }//GEN-LAST:event_downButtonEvent

    private void zoomInButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomInButtonEvent
        timelineDisplay.zoomIn();
    }//GEN-LAST:event_zoomInButtonEvent

    private void zoomOutButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zoomOutButtonEvent
        timelineDisplay.zoomOut();
    }//GEN-LAST:event_zoomOutButtonEvent

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JButton downButton;
    private javax.swing.JButton removeButton;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JToggleButton snapButton;
    private javax.swing.JButton upButton;
    private javax.swing.JButton zoomInButton;
    private javax.swing.JButton zoomOutButton;
    // End of variables declaration//GEN-END:variables
}
