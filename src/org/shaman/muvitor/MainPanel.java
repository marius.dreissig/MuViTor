/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.player.Player;

/**
 *
 * @author Sebastian
 */
public class MainPanel extends javax.swing.JPanel 
		implements PropertyChangeListener {
	private static final Logger LOG = Logger.getLogger(MainPanel.class.getName());
	
	private Project project;
	private Player player;
	private UndoableEditSupport undoSupport;
	private ArrayList<Consumer<Graphics2D>> extraDrawCommands;
	private AffineTransform transform = new AffineTransform();
	
	private Action playPauseAction;
	private Action highResPreviewAction;
	private Action enableFilterAction;
	
	private FrameTime currentTime;
	
	/**
	 * Creates new form MainPanel
	 */
	public MainPanel() {
		extraDrawCommands = new ArrayList<>();
		initComponents();
	}
	
	public JPanel getContentPanel() {
		return contentPanel;
	}
	public void addExtraDrawCommand(Consumer<Graphics2D> c) {
		extraDrawCommands.add(c);
	}
	
	public void initMenu(JMenuBar menu, JToolBar toolbar) {
		//create actions
		playPauseAction = new AbstractAction("play / pause", 
				new ImageIcon(MainPanel.class.getClassLoader().getResource("org/shaman/muvitor/icons/play16.png"))) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (player.isPlaying()) {
					stopEvent(null);
				} else {
					playEvent(null);
				}
			}
		};
		playPauseAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_P);
		playPauseAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, ActionEvent.CTRL_MASK));
		playPauseAction.setEnabled(false);
		
		highResPreviewAction = new AbstractAction("high resolution preview", 
				new ImageIcon(MainPanel.class.getClassLoader().getResource("org/shaman/muvitor/icons/hd16.png"))) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean selected = (boolean) getValue(Action.SELECTED_KEY);
				player.setHighResPreview(selected);
			}
		};
		highResPreviewAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(MainPanel.class.getClassLoader().getResource("org/shaman/muvitor/icons/hd24.png")));
		highResPreviewAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_H);
		highResPreviewAction.putValue(Action.SELECTED_KEY, false);
		highResPreviewAction.setEnabled(false);
		
		enableFilterAction = new AbstractAction("enable filters", 
				new ImageIcon(MainPanel.class.getClassLoader().getResource("org/shaman/muvitor/icons/filters16.png"))) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean selected = (boolean) getValue(Action.SELECTED_KEY);
				player.getFilterExecutor().setEnabled(selected);
			}
		};
		enableFilterAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(MainPanel.class.getClassLoader().getResource("org/shaman/muvitor/icons/filters24.png")));
		enableFilterAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_F);
		enableFilterAction.putValue(Action.SELECTED_KEY, true);
		enableFilterAction.setEnabled(false);
		
		//create menu
		JMenu playMenu = new JMenu("Play");
		playMenu.setMnemonic(KeyEvent.VK_P);
		menu.add(playMenu);
		
		playMenu.add(playPauseAction);
		playMenu.add(new JCheckBoxMenuItem(highResPreviewAction));
		playMenu.add(new JCheckBoxMenuItem(enableFilterAction));
		
		//add to toolbar
		//TODO: how to add the actions to the toolbar so that the text isn't displayed
		toolbar.addSeparator();
		JToggleButton b1 = new JToggleButton(highResPreviewAction);
		b1.setText(null);
		toolbar.add(b1);
		JToggleButton b2 = new JToggleButton(enableFilterAction);
		b2.setText(null);
		toolbar.add(b2);
	}

	public void setProject(Project project) {
		this.project = project;
		project.addPropertyChangeListener(this);
		currentTime = new FrameTime(project.getFramerate());
		playPauseAction.setEnabled(true);
		highResPreviewAction.setEnabled(true);
		enableFilterAction.setEnabled(true);
	}
	
	public void setUndoSupport(UndoableEditSupport undoSupport) {
		this.undoSupport = undoSupport;
	}

	public void setPlayer(Player player) {
		this.player = player;
		player.addPropertyChangeListener(this);
		setTotalTime(project.getLength());
		setCurrentTime(project.getTime());
		player.setRenderListener((FrameTime t) -> {
			SwingUtilities.invokeLater(contentPanel::repaint);
		});
		player.getFilterExecutor().setEnabled(enableFilterAction.isEnabled());
	}

	private void setPlaying(boolean playing) {
		playButton.setEnabled(!playing);
		timeSpinner.setEnabled(!playing);
		timeSlider.setEnabled(!playing);
	}
	
	/**
	 * Sets the total time in the UI
	 */
	private void setTotalTime(FrameTime msec) {
		LOG.log(Level.INFO, "set total time to {0} ms", msec);
		//check current time
		boolean fireTimeChange = false;
		if (currentTime.compareTo(msec) > 0) {
			currentTime = FrameTime.fromFrames(0, project.getFramerate());
			fireTimeChange = true;
		}
		
		timeLabel.setText("/ "+msec+" s");
		timeSpinner.setModel(currentTime.getSpinnerModel(FrameTime.fromFrames(0, project.getFramerate()), msec));
		timeSlider.setMinimum(0);
		timeSlider.setMaximum(msec.toMillis());
		timeSlider.setValue(currentTime.toMillis());
		
		if (fireTimeChange) {
			changeCurrentTime(currentTime);
		}
	}
	
	/**
	 * Sets the current time in the UI
	 */
	private void setCurrentTime(FrameTime msec) {
		if (msec == currentTime) return;
		currentTime = msec;
		timeSpinner.setValue(msec);
		timeSlider.setValue(msec.toMillis());
//		LOG.log(Level.INFO, "set current time to {0} ms", msec);
	}
	
	/**
	 * Sends the current time to the player
	 * @param time 
	 */
	private void changeCurrentTime(FrameTime time) {
		project.setTime(time);
		LOG.log(Level.INFO, "change current time to {0} ms", time);
	}
	
	private void paintContent(Graphics2D g) {
		if (player == null || project == null) {
			return;
		}
		if (player.isRecordAudio()) {
			return;
		}
		//transform graphics to view the whole screen
		int sw = contentPanel.getWidth();
		int sh = contentPanel.getHeight();
		int pw = project.getWidth();
		int ph = project.getHeight();
		float scale = Math.min(sw / (float) pw, sh / (float) ph);
		pw *= scale;
		ph *= scale;
		int ox = (sw - pw) / 2;
		int oy = (sh - ph) / 2;
		AffineTransform at = g.getTransform();
		g.translate(ox, oy);
		g.scale(scale, scale);
		transform.setTransform(g.getTransform());
		
		//send to player
		player.draw(g);
		
		//check focussed object, allow them to draw (filters)
		if (Selections.getInstance().getFocussedObject() instanceof ScreenInteractivable) {
			((ScreenInteractivable) Selections.getInstance().getFocussedObject()).drawOnScreen(g);
		}
		
		//reset transform
		g.setTransform(at);
	}
	
	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        contentPanel = new ContentPanel();
        playButton = new javax.swing.JButton();
        stopButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        timeSpinner = new javax.swing.JSpinner();
        timeLabel = new javax.swing.JLabel();
        timeSlider = new javax.swing.JSlider();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setMinimumSize(new java.awt.Dimension(400, 300));

        javax.swing.GroupLayout contentPanelLayout = new javax.swing.GroupLayout(contentPanel);
        contentPanel.setLayout(contentPanelLayout);
        contentPanelLayout.setHorizontalGroup(
            contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 568, Short.MAX_VALUE)
        );
        contentPanelLayout.setVerticalGroup(
            contentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 367, Short.MAX_VALUE)
        );

        playButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/play16.png"))); // NOI18N
        playButton.setToolTipText("Play");
        playButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playEvent(evt);
            }
        });

        stopButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/shaman/muvitor/icons/stop16.png"))); // NOI18N
        stopButton.setToolTipText("Stop");
        stopButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopEvent(evt);
            }
        });

        jLabel1.setText(" time:");

        timeSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 10));
        timeSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                timeSpinnerChanged(evt);
            }
        });

        timeLabel.setText("/ 0 s");

        timeSlider.setMajorTickSpacing(1000);
        timeSlider.setMinorTickSpacing(10);
        timeSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                timeSliderChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(contentPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(playButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stopButton)
                .addGap(2, 2, 2)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(timeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(timeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(timeSlider, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(contentPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(playButton)
                    .addComponent(stopButton)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timeSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void playEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playEvent
		player.startPlayback();
    }//GEN-LAST:event_playEvent

    private void stopEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopEvent
        if (player.isPlaying()) {
			player.stopPlayback();
		} else if (currentTime.toMillis() != 0) {
			FrameTime ft = new FrameTime(project.getFramerate());
			setCurrentTime(ft);
			changeCurrentTime(ft);
		}
    }//GEN-LAST:event_stopEvent

    private void timeSpinnerChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_timeSpinnerChanged
        FrameTime ft = (FrameTime) timeSpinner.getValue();
		if (ft.equals(currentTime)) {
			return;
		}
		currentTime = ft;
		timeSlider.setValue(ft.toMillis());
		changeCurrentTime(ft);
    }//GEN-LAST:event_timeSpinnerChanged

    private void timeSliderChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_timeSliderChanged
        int msec = timeSlider.getValue();
		FrameTime ft = FrameTime.fromMillis(msec, project.getFramerate());
		if (ft.equals(currentTime)) return;
		currentTime = ft;
		timeSpinner.setValue(ft);
		changeCurrentTime(ft);
    }//GEN-LAST:event_timeSliderChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel contentPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton playButton;
    private javax.swing.JButton stopButton;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JSlider timeSlider;
    private javax.swing.JSpinner timeSpinner;
    // End of variables declaration//GEN-END:variables

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (player == null) {
			return;
		}
		if (evt.getSource() == project) {
			if (player.isRecordFrames()) {
				return;
			}
			switch (evt.getPropertyName()) {
				case Project.PROP_LENGTH:
					setTotalTime(project.getLength());
					break;
				case Project.PROP_TIME:
					setCurrentTime(project.getTime());
					break;
			}
		} else if (evt.getSource() == player) {
			switch (evt.getPropertyName()) {
				case Player.PROP_PLAYING:
					setPlaying((boolean) evt.getNewValue());
					break;
			}
		}
	}
	
	private class ContentPanel extends JPanel
			implements MouseWheelListener, MouseMotionListener, MouseListener {

		private static final int DELTA_TIME_KEY = 1000;
		private long leftTime, rightTime = 0;
		
		public ContentPanel() {
			addMouseListener(this);
			addMouseMotionListener(this);
			addMouseWheelListener(this);
			setFocusable(true);
			
			InputMap im = getInputMap(WHEN_IN_FOCUSED_WINDOW);
			ActionMap am = getActionMap();
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "timeLeftPressed");
			am.put("timeLeftPressed", new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					keyPressed(true);
				}
			});
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0, true), "timeLeftReleased");
			am.put("timeLeftReleased", new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					keyReleased();
				}
			});
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "timeRightPressed");
			am.put("timeRightPressed", new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					keyPressed(false);
				}
			});
			im.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0, true), "timeRightReleased");
			am.put("timeRightReleased", new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent ae) {
					keyReleased();
				}
			});
		}
		
		@Override
		public void paint(Graphics g) {
			paintComponent(g);
			paintBorder(g);
			
			Graphics2D g2 = (Graphics2D) g.create();
			paintContent(g2);
			g2.dispose();
			
			for (Consumer<Graphics2D> c : extraDrawCommands) {
				c.accept((Graphics2D) g);
			}
			
			if (currentTime != null) {
				long t = System.currentTimeMillis();
				if (leftTime > 0 && t - leftTime > DELTA_TIME_KEY) {
					FrameTime ft = currentTime.clone().decrement();
					setCurrentTime(ft);
					changeCurrentTime(ft);
				}
				if (rightTime > 0 && t - rightTime > DELTA_TIME_KEY) {
					FrameTime ft = currentTime.clone().increment();
					setCurrentTime(ft);
					changeCurrentTime(ft);
				}
			}
		}

		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			if (Selections.getInstance().getFocussedObject() instanceof ScreenInteractivable) {
				((ScreenInteractivable) Selections.getInstance().getFocussedObject()).mouseWheelMoved(e);
			}
		}

		private MouseEvent convert(MouseEvent e) {
			Point2D p = new Point2D.Float();
			try {
				transform.inverseTransform(new Point2D.Float(e.getX(), e.getY()), p);
			} catch (NoninvertibleTransformException ex) {
				Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
			}
			MouseEvent ne = new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), 
					e.getModifiers(), (int) p.getX(), (int) p.getY(), 
					e.getXOnScreen(), e.getYOnScreen(), e.getClickCount(), 
					e.isPopupTrigger(), e.getButton());
			return ne;
		}
		
		@Override
		public void mouseDragged(MouseEvent e) {
			if (Selections.getInstance().getFocussedObject() instanceof ScreenInteractivable) {
				((ScreenInteractivable) Selections.getInstance().getFocussedObject()).mouseDragged(convert(e));
			}
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			if (Selections.getInstance().getFocussedObject() instanceof ScreenInteractivable) {
				((ScreenInteractivable) Selections.getInstance().getFocussedObject()).mouseMoved(convert(e));
			}
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			if (Selections.getInstance().getFocussedObject() instanceof ScreenInteractivable) {
				((ScreenInteractivable) Selections.getInstance().getFocussedObject()).mouseClicked(convert(e));
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (Selections.getInstance().getFocussedObject() instanceof ScreenInteractivable) {
				((ScreenInteractivable) Selections.getInstance().getFocussedObject()).mousePressed(convert(e));
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if (Selections.getInstance().getFocussedObject() instanceof ScreenInteractivable) {
				((ScreenInteractivable) Selections.getInstance().getFocussedObject()).mouseReleased(convert(e));
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			if (Selections.getInstance().getFocussedObject() instanceof ScreenInteractivable) {
				((ScreenInteractivable) Selections.getInstance().getFocussedObject()).mouseEntered(convert(e));
			}
		}

		@Override
		public void mouseExited(MouseEvent e) {
			if (Selections.getInstance().getFocussedObject() instanceof ScreenInteractivable) {
				((ScreenInteractivable) Selections.getInstance().getFocussedObject()).mouseExited(convert(e));
			}
		}

		public void keyPressed(boolean left) {
			if (left) {
				leftTime = System.currentTimeMillis();
				FrameTime t = currentTime.clone().decrement();
				setCurrentTime(t);
				changeCurrentTime(t);
			} else {
				rightTime = System.currentTimeMillis();
				FrameTime t = currentTime.clone().increment();
				setCurrentTime(t);
				changeCurrentTime(t);
			}
		}

		public void keyReleased() {
			leftTime = 0;
			rightTime = 0;
		}
		
	}
}
