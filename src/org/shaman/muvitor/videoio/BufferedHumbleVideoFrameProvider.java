/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.videoio;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A video frame provider that buffers the frames and allows to seek in the video
 * stream (but this is slow). It uses {@link HumbleVideoFrameProvider} internally.
 * It buffers future frames and loads them in a background thread.
 * @author Sebastian
 */
public class BufferedHumbleVideoFrameProvider implements VideoFrameProvider {

	private static final Logger LOG = Logger.getLogger(BufferedHumbleVideoFrameProvider.class.getName());
	
//	private static final int PAST_CACHE_CAPACITY = 10;
	private static final int FUTURE_CACHE_CAPACITY = 5;
	
	private final String url;
	private WorkerThread thread;
	
	private final int width;
	private final int height;
	private final int frameCount;
	
	private int currentIndex;
//	private ArrayDeque<BufferedImage> pastCache;
	private BufferedImage current;
	private LinkedBlockingDeque<Object> futureCache; //Object as null-marker
	
	
	public BufferedHumbleVideoFrameProvider(String url) throws InterruptedException, IOException {
		this.url = url;
		//caches
		currentIndex = 0;
//		pastCache = new ArrayDeque<>(PAST_CACHE_CAPACITY);
		futureCache = new LinkedBlockingDeque<>(FUTURE_CACHE_CAPACITY);
		
		//open for the first time, to check for exceptions
		HumbleVideoFrameProvider delegate = new HumbleVideoFrameProvider(url);
		width = delegate.getWidth();
		height = delegate.getHeight();
		frameCount = delegate.getFrameCount();
		
		//start background thread
		thread = new WorkerThread(delegate);
		thread.start();
		current = take();
	}
	private BufferedImage take() throws InterruptedException {
		Object o = futureCache.takeFirst();
		if (o instanceof BufferedImage) {
			return (BufferedImage) o;
		} else {
			return null;
		}
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getFrameCount() {
		return frameCount;
	}

	@Override
	public int getCurrentFrameIndex() {
		return currentIndex;
	}

	@Override
	public BufferedImage getCurrentFrame(BufferedImage toFill) {
		return current;
	}

	@Override
	public void advance() {
		if (current == null) {
			return; //already at the end
		}
		try {
			current = take();
			currentIndex++;
		} catch (InterruptedException ex) {
			Logger.getLogger(BufferedHumbleVideoFrameProvider.class.getName()).log(Level.SEVERE, null, ex);
			current = null;
		}
	}

	@Override
	public boolean supportsSeeking() {
		return true;
	}

	@Override
	public boolean setCurrentFrameIndex(int newIndex) {
		if (newIndex > currentIndex) {
			//seek forward
			if (newIndex > currentIndex+1) //stepping one step forward is so common (it is just an advance(), don't log it
				LOG.log(Level.INFO, "seek forward from {0} to {1}", new Object[]{currentIndex, newIndex});
			for (int i=currentIndex; i<newIndex; ++i) {
				advance();
			}
		}
		if (newIndex < currentIndex) {
			try {
				//seek backward
				LOG.info("seek backward from "+currentIndex+" to "+newIndex+". Worker must be restarted, this is slow");
				//stop current worker
				thread.interrupt();
				thread.join();
				//clear queue
				futureCache.clear();
				current = null;
				//start new worker
				HumbleVideoFrameProvider delegate = new HumbleVideoFrameProvider(url);
				thread = new WorkerThread(delegate);
				thread.start();
				current = take();
				currentIndex = 0;
				//seek forward
				for (int i=0; i<newIndex; ++i) {
					advance();
				}
			} catch (InterruptedException | IOException ex) {
				LOG.log(Level.SEVERE, "Unable to restart worker thread", ex);
				return false;
			}
		}
		return current != null; //end not reached
	}
	
	private class WorkerThread extends Thread {
		private final HumbleVideoFrameProvider delegate;

		public WorkerThread(HumbleVideoFrameProvider delegate) {
			this.delegate = delegate;
		}

		@Override
		public void run() {
			while(true) {
				BufferedImage img = delegate.getCurrentFrame(null);
				try {
					if (img == null) {
						futureCache.putLast(new Object());
					} else {
						futureCache.putLast(copyImage(img));
					}
				} catch (InterruptedException ex) {
					try {
						delegate.close();
					} catch (InterruptedException | IOException ex2) {
						Logger.getLogger(BufferedHumbleVideoFrameProvider.class.getName()).log(Level.SEVERE, null, ex2);
					}
					break; //interrupted, thread should be restarted
				}
				if (img == null) {
					break; //end reached
				}
				delegate.advance();
			}
		}
		
		
	}
	
	public static BufferedImage copyImage(BufferedImage bi) {
		ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
}
