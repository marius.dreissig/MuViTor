/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.videoio;

import io.humble.video.Decoder;
import io.humble.video.Demuxer;
import io.humble.video.DemuxerStream;
import io.humble.video.Global;
import io.humble.video.MediaDescriptor;
import io.humble.video.MediaPacket;
import io.humble.video.MediaPicture;
import io.humble.video.Rational;
import io.humble.video.awt.MediaPictureConverter;
import io.humble.video.awt.MediaPictureConverterFactory;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implements {@link VideoFrameProvider} using humble video.
 * Seeking is not supported
 * @author Sebastian
 */
public class HumbleVideoFrameProvider implements VideoFrameProvider {

	private static final Logger LOG = Logger.getLogger(HumbleVideoFrameProvider.class.getName());

	private Demuxer demuxer;
	private Decoder videoDecoder;
	private MediaPicture picture;
	private MediaPictureConverter converter;
	private BufferedImage image;
	private MediaPacket packet;
	private int videoStreamId;
	
	private int width;
	private int height;
	private int frameIndex;
	
	private int offset;
	private int bytesRead;
	private boolean packetEmpty = false;
	private boolean open = false;
	
	public HumbleVideoFrameProvider(String url) throws InterruptedException, IOException {
		open(url);
	}
	
	private void open(String filename) throws InterruptedException, IOException {
		demuxer = Demuxer.make();

		/*
		 * Open the demuxer with the filename passed on.
		 */
		demuxer.open(filename, null, false, true, null, null);

		/*
		 * Query how many streams the call to open found
		 */
		int numStreams = demuxer.getNumStreams();

		/*
		 * Iterate through the streams to find the first video stream
		 */
		videoStreamId = -1;
		long streamStartTime = Global.NO_PTS;
		videoDecoder = null;
		for (int i = 0; i < numStreams; i++) {
			final DemuxerStream stream = demuxer.getStream(i);
			streamStartTime = stream.getStartTime();
			final Decoder decoder = stream.getDecoder();
			if (decoder != null && decoder.getCodecType() == MediaDescriptor.Type.MEDIA_VIDEO) {
				videoStreamId = i;
				videoDecoder = decoder;
				// stop at the first one.
				break;
			}
		}
		if (videoStreamId == -1) {
			throw new IOException("could not find video stream in container: " + filename);
		}

		/*
		 * Now we have found the audio stream in this file.  Let's open up our decoder so it can
		 * do work.
		 */
		videoDecoder.open(null, null);

		picture = MediaPicture.make(
				videoDecoder.getWidth(),
				videoDecoder.getHeight(),
				videoDecoder.getPixelFormat());

		/**
		 * A converter object we'll use to convert the picture in the video to a
		 * BGR_24 format that Java Swing can work with. You can still access the
		 * data directly in the MediaPicture if you prefer, but this abstracts
		 * away from this demo most of that byte-conversion work. Go read the
		 * source code for the converters if you're a glutton for punishment.
		 */
		converter
				= MediaPictureConverterFactory.createConverter(
						MediaPictureConverterFactory.HUMBLE_BGR_24,
						picture);
		image = null;
		
		/*
		 * create packet
		 */
		packet = MediaPacket.make();
		
		width = videoDecoder.getWidth();
		height = videoDecoder.getHeight();
		open = true;
		
		//load first frame
		frameIndex = -1;
		next();
	}
	
	public void close() throws InterruptedException, IOException {
		demuxer.close();
		open = false;
	}
	
	private void next() throws InterruptedException, IOException {
		if (!open) {
			throw new IllegalStateException("no more images available");
		}
		if (!packetEmpty) {
			while (demuxer.read(packet) >= 0) {
				/**
				 * Now we have a packet, let's see if it belongs to our video stream
				 */
				if (packet.getStreamIndex() == videoStreamId) {
					/**
					 * A packet can actually contain multiple sets of samples (or
					 * frames of samples in decoding speak). So, we may need to call
					 * decode multiple times at different offsets in the packet's
					 * data. We capture that here.
					 */
					offset = 0;
					bytesRead = 0;
					do {
						bytesRead += videoDecoder.decode(picture, packet, offset);
						if (picture.isComplete()) {
							image = converter.toImage(image, picture);
							frameIndex++;
//							System.out.println("frame "+frameIndex+", timestep "+picture.getTimeStamp()+", unit "+videoDecoder.getTimeBase());
							return;
						}
						offset += bytesRead;
					} while (offset < packet.getSize());
				}
			}
			packetEmpty = true;
		}

		if (packetEmpty) {
			// Some video decoders (especially advanced ones) will cache
			// video data before they begin decoding, so when you are done you need
			// to flush them. The convention to flush Encoders or Decoders in Humble Video
			// is to keep passing in null until incomplete samples or packets are returned.
			do {
				videoDecoder.decode(picture, null, 0);
				if (picture.isComplete()) {
					image = converter.toImage(image, picture);
					frameIndex++;
					return;
				}
			} while (picture.isComplete());
			close();
		}
	}
	
	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getFrameCount() {
		return -1;
	}

	@Override
	public int getCurrentFrameIndex() {
		return frameIndex;
	}

	@Override
	public BufferedImage getCurrentFrame(BufferedImage toFill) {
		if (!open) {
			return null;
		}
		return image;
	}

	@Override
	public void advance() {
		try {
			next();
		} catch (InterruptedException | IOException ex) {
			LOG.log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public boolean supportsSeeking() {
		return false;
	}

	@Override
	public boolean setCurrentFrameIndex(int newIndex) {
		throw new UnsupportedOperationException("Seeking is not supported.");
	}
	
}
