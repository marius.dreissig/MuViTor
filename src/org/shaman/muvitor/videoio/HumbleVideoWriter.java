/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.videoio;

import io.humble.video.Codec;
import io.humble.video.Encoder;
import io.humble.video.MediaPacket;
import io.humble.video.MediaPicture;
import io.humble.video.Muxer;
import io.humble.video.MuxerFormat;
import io.humble.video.PixelFormat;
import io.humble.video.Rational;
import io.humble.video.awt.MediaPictureConverter;
import io.humble.video.awt.MediaPictureConverterFactory;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Logger;

/**
 *
 * @author Sebastian
 */
public class HumbleVideoWriter implements AutoCloseable {

	private static final Logger LOG = Logger.getLogger(HumbleVideoWriter.class.getName());

	private final Muxer muxer;
	private final Encoder encoder;
	private MediaPictureConverter converter;
	private final MediaPicture picture;
	private final MediaPacket packet;
	private int step;
	private final long stepToMicro;
	
	public HumbleVideoWriter(String url, int framePerSecond, int width, int height,
			String formatname, String codecname) throws InterruptedException, IOException {
		//make muxer
		muxer = Muxer.make(url, null, formatname);

		//create encoder
		MuxerFormat muxerFormat = muxer.getFormat();
		Codec codec = null;
		if (codecname != null) {
			codec = Codec.findEncodingCodecByName(codecname);
			if (codec == null) {
				LOG.warning("unsupported codec: "+codecname);
			}
		}
		if (codec==null) {
			codec = Codec.findEncodingCodec(muxerFormat.getDefaultVideoCodecId());
		}
		encoder = Encoder.make(codec);

		//setup encoder
		encoder.setWidth(width);
		encoder.setHeight(height);
		PixelFormat.Type pixelformat = PixelFormat.Type.PIX_FMT_YUV420P;
		encoder.setPixelFormat(pixelformat);
		Rational framerate = Rational.make(1, framePerSecond);
		encoder.setTimeBase(framerate);
		if (muxerFormat.getFlag(MuxerFormat.Flag.GLOBAL_HEADER)) {
			encoder.setFlag(Encoder.Flag.FLAG_GLOBAL_HEADER, true);
		}

		//open the encoder
		encoder.open(null, null);

		//add stream to muxer
		muxer.addNewStream(encoder);

		//open muxer
		muxer.open(null, null);

		//setup picture
		picture = MediaPicture.make(
				encoder.getWidth(),
				encoder.getHeight(),
				pixelformat);
		picture.setTimeBase(framerate);

		//make packet
		packet = MediaPacket.make();
		
		//setup steps to micro
		stepToMicro = 1000000 / framePerSecond;
	}
	
	public void addImage(BufferedImage img) {
		img = convertToType(img, BufferedImage.TYPE_3BYTE_BGR);
		if (converter == null) {
			converter = MediaPictureConverterFactory.createConverter(img, picture);
		}
		converter.toPicture(picture, img, step/* * stepToMicro*/);
		step++;

		do {
			encoder.encode(packet, picture);
			if (packet.isComplete()) {
				muxer.write(packet, false);
			}
		} while (packet.isComplete());
	}

	@Override
	public void close() throws Exception {
		//flush encoder
		do {
			encoder.encode(packet, null);
			if (packet.isComplete()) {
				muxer.write(packet, false);
			}
		} while (packet.isComplete());
		
		//close
		muxer.close();
	}

	/**
	 * Convert a {@link BufferedImage} of any type, to {@link BufferedImage} of
	 * a specified type. If the source image is the same type as the target
	 * type, then original image is returned, otherwise new image of the correct
	 * type is created and the content of the source image is copied into the
	 * new image.
	 *
	 * @param sourceImage the image to be converted
	 * @param targetType the desired BufferedImage type
	 *
	 * @return a BufferedImage of the specifed target type.
	 *
	 * @see BufferedImage
	 */
	private static BufferedImage convertToType(BufferedImage sourceImage,
			int targetType) {
		BufferedImage image;

		// if the source image is already the target type, return the source image
		if (sourceImage.getType() == targetType) {
			image = sourceImage;
		} // otherwise create a new image of the target type and draw the new
		// image
		else {
			image = new BufferedImage(sourceImage.getWidth(),
					sourceImage.getHeight(), targetType);
			image.getGraphics().drawImage(sourceImage, 0, 0, null);
		}

		return image;
	}

}
