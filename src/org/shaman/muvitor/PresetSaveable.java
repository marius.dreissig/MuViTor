/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

/**
 * If an object that implements this interface is edited in the property panel,
 * then buttons to load and save presets are displayed.
 * @author Sebastian Weiss
 * @param <T> the type of the structure holding the properties
 */
public interface PresetSaveable<T> {
	/**
	 * Called when the preset should be saved / copied
	 * @return the structure to save / copy
	 */
	T savePresetSettings();
	
	/**
	 * Called when the preset should be apply / pasted
	 * @param settings the structure to apply / paste
	 */
	void loadPresetSettings(T settings);
	
	/**
	 * The class of the presets, needed for deserialization
	 * @return 
	 */
	Class<?> presetClass();
}
