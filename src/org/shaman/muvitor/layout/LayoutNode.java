/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.layout;

import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Objects;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.Listenable;
import org.shaman.muvitor.timeline.ImageTimelineObject;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.ElementList;

/**
 * A node in the binary layout tree, both inner node and leaf.
 * A node is a leaf iff {@link #children} is null. Then it can contain a timeline
 * object in {@link #leafObject}. If the node is an inner node, then 
 * {@link #children} is exactly of length two. 
 * @author Sebastian
 */
public final class LayoutNode implements Listenable {
	
	protected UndoableEditSupport undoSupport;
	protected transient PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	
	@ElementArray(required = false)
	private LayoutNode[] children;
	
	private LayoutNode parent;
	private Layout layout;
	
	public static final String PROP_SPLITTED = "splitted";
	public static final String PROP_MERGED = "merged";
	
	public static final String PROP_LEAF_OBJECT = "leafObject";
	@Element(required = false)
	private ImageTimelineObject leafObject;
	
	public static enum SplitDirection {
		VERTICALLY,
		HORIZONTALLY
	}
	
	@Element
	private float splitPoint = 0.5f;
	public static final String PROP_SPLITPOINT = "splitPoint";
	
	@Element
	private SplitDirection splitDirection = SplitDirection.HORIZONTALLY;
	public static final String PROP_SPLITDIRECTION = "splitDirection";
	
	@Element
	private float zoomFactor = 1.0f;
	public static final String PROP_ZOOMFACTOR = "zoomFactor";
	
	@Element
	private int translationX = 0;
	public static final String PROP_TRANSLATIONX = "translationX";
	
	@Element
	private int translationY = 0;
	public static final String PROP_TRANSLATIONY = "translationY";

	
	//for the layout panel
	final Rectangle layoutedRect = new Rectangle();

	/**
	 * Serialization
	 */
	public LayoutNode() {
		splitPoint = 0.5f;
	}
	/**
	 * Creates a new leaf node
	 */
	public LayoutNode(Layout layout, UndoableEditSupport undoSupport) {
		splitPoint = 0.5f;
		init(layout, undoSupport);
	}

	public void init(Layout layout, UndoableEditSupport undoSupport) {
		this.layout = layout;
		this.undoSupport = undoSupport;
		if (children != null) {
			for (LayoutNode n : children) {
				n.parent = this;
				n.init(layout, undoSupport);
			}
		}
	}
	
	LayoutNode cloneForSaving() {
		LayoutNode n = new LayoutNode();
		n.splitPoint = splitPoint;
		n.splitDirection = splitDirection;
		n.translationX = translationX;
		n.translationY = translationY;
		n.zoomFactor = zoomFactor;
		if (!isLeaf()) {
			n.children = new LayoutNode[] {
				children[0].cloneForSaving(),
				children[1].cloneForSaving(),
			};
		}
		return n;
	}
	
	private void fireChangeInLayout() {
		if (layout != null) {
			layout.fireNodeChanged(this);
		}
	}
	
	/**
	 * @return {@code true} iff this node is a leaf node
	 */
	public boolean isLeaf() {
		return children == null;
	}
	
	protected void assertLeaf() {
		if (!isLeaf()) {
			throw new IllegalStateException("node must be a leaf");
		}
	}
	protected void assertInner() {
		if (isLeaf()) {
			throw new IllegalStateException("node must be an inner node");
		}
	}
	
	/**
	 * Returns the timeline object referenced from a leaf node.
	 * @return 
	 */
	public ImageTimelineObject getLeafObject() {
		assertLeaf();
		return leafObject;
	}
	
	public void setLeafObjectNoUndo(ImageTimelineObject newLeafObject) {
		assertLeaf();
		ImageTimelineObject oldLeafObject = leafObject;
		leafObject = newLeafObject;
		propertyChangeSupport.firePropertyChange(PROP_LEAF_OBJECT, oldLeafObject, newLeafObject);
		fireChangeInLayout();
	}
	
	/**
	 * Sets the timeline object referenced from the leaf node.
	 * @param newLeafObject 
	 */
	public void setLeafObject(ImageTimelineObject newLeafObject) {
		ImageTimelineObject oldLeafObject = leafObject;
		setLeafObjectNoUndo(newLeafObject);
		if (!Objects.equals(oldLeafObject, newLeafObject) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					leafObject = oldLeafObject;
					propertyChangeSupport.firePropertyChange(PROP_LEAF_OBJECT, newLeafObject, oldLeafObject);
					fireChangeInLayout();
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					leafObject = newLeafObject;
					propertyChangeSupport.firePropertyChange(PROP_LEAF_OBJECT, oldLeafObject, newLeafObject);
					fireChangeInLayout();
				}
			
			});
		}
	}
	
	/**
	 * Get the value of zoomFactor
	 *
	 * @return the value of zoomFactor
	 */
	public float getZoomFactor() {
		return zoomFactor;
	}

	/**
	 * Set the value of zoomFactor
	 *
	 * @param zoomFactor new value of zoomFactor
	 */
	public void setZoomFactorNoUndo(float zoomFactor) {
		float oldZoomFactor = this.zoomFactor;
		this.zoomFactor = Math.max(1, zoomFactor);
		propertyChangeSupport.firePropertyChange(PROP_ZOOMFACTOR, oldZoomFactor, zoomFactor);
		fireChangeInLayout();
	}
	
	public void fireZoomFactorUndoRedo(float oldZoomFactor, float newZoomFactor) {
		if (oldZoomFactor!=newZoomFactor && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					zoomFactor = oldZoomFactor;
					propertyChangeSupport.firePropertyChange(PROP_ZOOMFACTOR, newZoomFactor, oldZoomFactor);
					fireChangeInLayout();
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					zoomFactor = newZoomFactor;
					propertyChangeSupport.firePropertyChange(PROP_ZOOMFACTOR, oldZoomFactor, newZoomFactor);
					fireChangeInLayout();
				}
			
			});
		}
	}
	
	public void setZoomFactor(float newZoomFactor2) {
		float oldZoomFactor = this.zoomFactor;
		float newZoomFactor = Math.max(1, newZoomFactor2);
		setZoomFactorNoUndo(newZoomFactor);
		fireZoomFactorUndoRedo(oldZoomFactor, newZoomFactor);
	}

	/**
	 * Get the value of translationX
	 *
	 * @return the value of translationX
	 */
	public int getTranslationX() {
		return translationX;
	}

	/**
	 * Set the value of translationX
	 *
	 * @param translationX new value of translationX
	 */
	public void setTranslationXNoUndo(int translationX) {
		int oldTranslationX = this.translationX;
		this.translationX = translationX;
		propertyChangeSupport.firePropertyChange(PROP_TRANSLATIONX, oldTranslationX, translationX);
		fireChangeInLayout();
	}

	public void fireTranslationXUndoRedo(int oldTranslationX, int newTranslationX) {
		if (oldTranslationX!=newTranslationX && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					translationX = oldTranslationX;
					propertyChangeSupport.firePropertyChange(PROP_TRANSLATIONX, newTranslationX, oldTranslationX);
					fireChangeInLayout();
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					translationX = newTranslationX;
					propertyChangeSupport.firePropertyChange(PROP_TRANSLATIONX, oldTranslationX, newTranslationX);
					fireChangeInLayout();
				}
			
			});
		}
	}
	
	/**
	 * Set the value of translationX
	 *
	 * @param newTranslationX new value of translationX
	 */
	public void setTranslationX(int newTranslationX) {
		int oldTranslationX = this.translationX;
		setTranslationXNoUndo(newTranslationX);
		fireTranslationXUndoRedo(oldTranslationX, newTranslationX);
	}
	
	/**
	 * Get the value of translationY
	 *
	 * @return the value of translationY
	 */
	public int getTranslationY() {
		return translationY;
	}

	/**
	 * Set the value of translationY
	 *
	 * @param translationY new value of translationY
	 */
	public void setTranslationYNoUndo(int translationY) {
		int oldTranslationY = this.translationY;
		this.translationY = translationY;
		propertyChangeSupport.firePropertyChange(PROP_TRANSLATIONY, oldTranslationY, translationY);
		fireChangeInLayout();
	}

	public void fireTranslationYUndoRedo(int oldTranslationY, int newTranslationY) {
		if (oldTranslationY!=newTranslationY && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					translationY = oldTranslationY;
					propertyChangeSupport.firePropertyChange(PROP_TRANSLATIONY, newTranslationY, oldTranslationY);
					fireChangeInLayout();
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					translationY = newTranslationY;
					propertyChangeSupport.firePropertyChange(PROP_TRANSLATIONY, oldTranslationY, newTranslationY);
					fireChangeInLayout();
				}
			
			});
		}
	}
	
	/**
	 * Set the value of translationY
	 *
	 * @param newTranslationY new value of translationY
	 */
	public void setTranslationY(int newTranslationY) {
		int oldTranslationY = this.translationY;
		setTranslationYNoUndo(newTranslationY);
		fireTranslationYUndoRedo(oldTranslationY, newTranslationY);
	}
	
	/**
	 * Returns the two child nodes
	 * @return the two child nodes
	 */
	public LayoutNode[] getChildren() {
		assertInner();
		return children;
	}
	
	/**
	 * Splits this leaf node into half
	 * @param direction the split direction
	 */
	public void split(SplitDirection direction) {
		assertLeaf();
		
		splitDirection = direction;
		final ImageTimelineObject obj = leafObject;
		if (obj != null) {
			obj.setEnabledNoUndo(false);
		}
		
		LayoutNode[] cx = new LayoutNode[2];
		for (int i=0; i<2; ++i) {
			cx[i] = new LayoutNode(layout, undoSupport);
			cx[i].parent = this;
		}
		
		leafObject = null;
		children = cx;
		propertyChangeSupport.firePropertyChange(PROP_SPLITTED, null, cx);
		fireChangeInLayout();
		
		if (undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {
				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					leafObject = obj;
					if (obj != null) {
						obj.setEnabledNoUndo(true);
					}
					children = null;
					propertyChangeSupport.firePropertyChange(PROP_MERGED, cx, null);
					fireChangeInLayout();
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					leafObject = null;
					if (obj != null) {
						obj.setEnabledNoUndo(false);
					}
					children = cx;
					propertyChangeSupport.firePropertyChange(PROP_SPLITTED, null, cx);
					fireChangeInLayout();
				}
				
			});
		}
	}
	
	private void disableObjectsRec() {
		if (leafObject != null) {
			leafObject.setEnabled(false);
		}
		if (!isLeaf()) {
			for (LayoutNode c : children) {
				c.disableObjectsRec();
			}
		}
	}
	/**
	 * Merges this inner node and discards all children
	 */
	public void merge() {
		assertInner();
		
		LayoutNode[] cx = children;
		disableObjectsRec();
		children = null;
		propertyChangeSupport.firePropertyChange(PROP_MERGED, cx, null);
		fireChangeInLayout();
		if (undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {
				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					children = cx;
					propertyChangeSupport.firePropertyChange(PROP_SPLITTED, null, cx);
					fireChangeInLayout();
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					disableObjectsRec();
					children = null;
					propertyChangeSupport.firePropertyChange(PROP_MERGED, cx, null);
					fireChangeInLayout();
				}
				
			});
		}
	}

	/**
	 * Get the value of splitPoint
	 *
	 * @return the value of splitPoint
	 */
	public float getSplitPoint() {
		return splitPoint;
	}
	
	/**
	 * Set the value of splitPoint without triggering an undo event
	 *
	 * @param splitPoint new value of splitPoint
	 */
	public void setSplitPointNoUndo(float splitPoint) {
		float oldSplitPoint = this.splitPoint;
		this.splitPoint = splitPoint;
		propertyChangeSupport.firePropertyChange(PROP_SPLITPOINT, oldSplitPoint, splitPoint);
		fireChangeInLayout();
	}
	
	/**
	 * Set the value of splitPoint
	 *
	 * @param splitPoint new value of splitPoint
	 */
	public void setSplitPoint(float splitPoint) {
		float oldSplitPoint = this.splitPoint;
		setSplitPointNoUndo(splitPoint);
		fireSplitPointUndoRedo(oldSplitPoint, splitPoint);
	}
	
	public void fireSplitPointUndoRedo(float oldSplitPoint, float newSplitPoint) {
		if (oldSplitPoint!=newSplitPoint && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					splitPoint = oldSplitPoint;
					propertyChangeSupport.firePropertyChange(PROP_SPLITPOINT, newSplitPoint, oldSplitPoint);
					fireChangeInLayout();
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					splitPoint = newSplitPoint;
					propertyChangeSupport.firePropertyChange(PROP_SPLITPOINT, oldSplitPoint, newSplitPoint);
					fireChangeInLayout();
				}
			
			});
		}
	}

	/**
	 * Get the value of splitDirection
	 *
	 * @return the value of splitDirection
	 */
	public SplitDirection getSplitDirection() {
		return splitDirection;
	}

	/**
	 * Set the value of splitDirection
	 *
	 * @param newSplitDirection new value of splitDirection
	 */
	public void setSplitDirection(SplitDirection newSplitDirection) {
		SplitDirection oldSplitDirection = this.splitDirection;
		this.splitDirection = newSplitDirection;
		propertyChangeSupport.firePropertyChange(PROP_SPLITDIRECTION, oldSplitDirection, newSplitDirection);
		fireChangeInLayout();
		if (!Objects.equals(oldSplitDirection, newSplitDirection) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					splitDirection = oldSplitDirection;
					propertyChangeSupport.firePropertyChange(PROP_SPLITDIRECTION, newSplitDirection, oldSplitDirection);
					fireChangeInLayout();
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					splitDirection = newSplitDirection;
					propertyChangeSupport.firePropertyChange(PROP_SPLITDIRECTION, oldSplitDirection, newSplitDirection);
					fireChangeInLayout();
				}
			
			});
		}
	}

	
	/**
	 * Add PropertyChangeListener.
	 *
	 * @param listener
	 */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * Remove PropertyChangeListener.
	 *
	 * @param listener
	 */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}
}
